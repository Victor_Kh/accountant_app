export default class UserDto {
  constructor({ name, email, id }) {
    this.name = name;
    this.email = email;
    this.id = id;
  }
}
