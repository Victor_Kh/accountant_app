import express from 'express';
import cookieParser from 'cookie-parser';
import cors from 'cors';
import dotenv from 'dotenv';
import sequelize from './utils/database.js';
import router from './routes/index.js';
import errorMiddleware from './middleware/errror.js';

dotenv.config();
const app = express();
app.use(express.json());
app.use(cookieParser());
app.use(cors({
  credentials: true,
  origin: 'http://localhost:3000',
}));
app.use('/', router);
app.use(errorMiddleware);
const PORT = process.env.PORT || 3001;

const startApp = async () => {
  try {
    // {force:true}
    await sequelize.sync();
    app.listen(PORT, () => { console.log(`App has been started on the port ${PORT}`); });
  } catch (e) {
    console.log(e);
  }
};

startApp();
