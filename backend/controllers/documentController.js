import jwt from 'jsonwebtoken';
import documentService from '../services/documentService.js';

class DocumentController {
  async addContract(req, res, next) {
    try {
      const { refreshToken } = req.cookies;
      const userData = jwt.decode(refreshToken);
      const { id: UserId } = userData;
      const contract = req.body;
      const contractData = await documentService.addContract(contract, UserId);
      return res.json(contractData);
    } catch (e) {
      next(e);
    }
  }

  async removeContract(req, res, next) {
    try {
      const { id } = req.params;
      const contractData = await documentService.removeContract(id);
      return res.json(contractData);
    } catch (e) {
      next(e);
    }
  }

  async updateContract(req, res, next) {
    try {
      const updatedContract = req.body;
      const { id } = req.params;
      const contractData = await documentService.updateContract(updatedContract, id);
      return res.json(contractData);
    } catch (e) {
      next(e);
    }
  }

  async getContract(req, res, next) {
    try {
      const { id } = req.params;
      const contract = await documentService.getContract(id);
      return res.json(contract);
    } catch (e) {
      next(e);
    }
  }

  async addCompletedWorkAct(req, res, next) {
    try {
      const { refreshToken } = req.cookies;
      const userData = jwt.decode(refreshToken);
      const { id: UserId } = userData;
      const completedWorkAct = req.body;
      const completedWorkActData = await documentService.addCompletedWorkAct(completedWorkAct, UserId);
      return res.json(completedWorkActData);
    } catch (e) {
      next(e);
    }
  }

  async removeCompletedWorkAct(req, res, next) {
    try {
      const { id } = req.params;
      const contractData = await documentService.removeCompletedWorkAct(id);
      return res.json(contractData);
    } catch (e) {
      next(e);
    }
  }

  async updateCompletedWorkAct(req, res, next) {
    try {
      const updatedCompletedWorkAct = req.body;
      const { id } = req.params;
      const completedWorkActData = await documentService.updateCompletedWorkAct(updatedCompletedWorkAct, id);
      return res.json(completedWorkActData);
    } catch (e) {
      next(e);
    }
  }

  async getCompletedWorkAct(req, res, next) {
    try {
      const { id } = req.params;
      const completedWorkAct = await documentService.getCompletedWorkAct(id);
      return res.json(completedWorkAct);
    } catch (e) {
      next(e);
    }
  }

  async getAllDocuments(req, res, next) {
    try {
      const { refreshToken } = req.cookies;
      const userData = jwt.decode(refreshToken);
      const { id: UserId } = userData;
      let {
        limit, page, order, search, startDate, endDate,
      } = req.query;
      limit = +limit || 10;
      page = page || 1;
      const offset = page * limit - limit;
      const allDocuments = await documentService
        .getAllDocuments(UserId, limit, offset, order, search, startDate, endDate);
      return res.json(allDocuments);
    } catch (e) {
      next(e);
    }
  }

  async getContractsByPartnersId(req, res, next) {
    try {
      const { id } = req.params;
      const contracts = await documentService.getContractsByPartnerId(id);
      return res.json(contracts);
    } catch (e) {
      next(e);
    }
  }
}

export default new DocumentController();
