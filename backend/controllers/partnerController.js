import jwt from 'jsonwebtoken';
import partnerService from '../services/partnerService.js';

class DocumentController {
  async removePartner(req, res, next) {
    try {
      const { id } = req.params;
      const partnerData = await partnerService.removePartner(id);
      return res.json(partnerData);
    } catch (e) {
      next(e);
    }
  }

  async updatePartner(req, res, next) {
    try {
      const updatedPartner = req.body;
      const { id } = req.params;
      updatedPartner.managersInfo = JSON.parse(updatedPartner.managersInfo);
      updatedPartner.deletedManagers = JSON.parse(updatedPartner.deletedManagers);
      updatedPartner.updatedManagers = JSON.parse(updatedPartner.updatedManagers);
      updatedPartner.banksInfo = JSON.parse(updatedPartner.banksInfo);
      updatedPartner.deletedBanks = JSON.parse(updatedPartner.deletedBanks);
      updatedPartner.updatedBanks = JSON.parse(updatedPartner.updatedBanks);
      const partnerData = await partnerService.updatePartner(updatedPartner, id);
      return res.json(partnerData);
    } catch (e) {
      next(e);
    }
  }

  async addPartner(req, res, next) {
    try {
      const { refreshToken } = req.cookies;
      const userData = jwt.decode(refreshToken);
      const { id: UserId } = userData;
      const partner = req.body;
      partner.banksInfo = JSON.parse(partner.banksInfo);
      partner.managersInfo = JSON.parse(partner.managersInfo);
      const partnerData = await partnerService.addPartner(partner, UserId);
      return res.json(partnerData);
    } catch (e) {
      next(e);
    }
  }

  async getPartner(req, res, next) {
    try {
      const { id } = req.params;
      const partner = await partnerService.getPartner(id);
      return res.json(partner);
    } catch (e) {
      next(e);
    }
  }

  async getAllPartners(req, res, next) {
    try {
      const { refreshToken } = req.cookies;
      const userData = jwt.decode(refreshToken);
      const { id: UserId } = userData;
      let {
        limit, page, search,
      } = req.query;
      limit = +limit || 10;
      page = page || 1;
      const offset = page * limit - limit;
      const allDocuments = await partnerService.getAllPartners(UserId, limit, offset, search);
      return res.json(allDocuments);
    } catch (e) {
      next(e);
    }
  }
}
export default new DocumentController();
