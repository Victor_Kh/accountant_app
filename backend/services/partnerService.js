import { Op } from 'sequelize';
import { Managers, Partner, PartnerBanks } from '../models/index.js';
import sequeilize from '../utils/database.js';

class PartnerService {
  async addPartner(partner, UserId) {
    let transaction;
    try {
      transaction = await sequeilize.transaction();
      const newPartner = await Partner.create({ ...partner, UserId }, { transaction });
      const { managersInfo, banksInfo } = partner;
      const createManagersPromises = managersInfo
        .map((managerInfo) => Managers.create({ ...managerInfo, PartnerId: newPartner.id }, { transaction }));
      const createPartnersBanksPromises = banksInfo
        .map((bankInfo) => PartnerBanks.create({ ...bankInfo, PartnerId: newPartner.id }, { transaction }));
      await Promise.all([...createManagersPromises, createPartnersBanksPromises]);
      await transaction.commit();
      return newPartner;
    } catch (e) {
      if (transaction) {
        await transaction.rollback();
      }
      throw e;
    }
  }

  updatePartnerManagers(updatedPartner, id, transaction) {
    const { deletedManagers, updatedManagers, managersInfo } = updatedPartner;
    const deleteManagerPromises = deletedManagers
      .map((deletedManager) => Managers.destroy({ where: { id: deletedManager.id }, transaction }));
    const updateManagerPromises = updatedManagers.map((managerInfo) => Managers.update({
      name: managerInfo.name,
      position: managerInfo.position,
      actingBy: managerInfo.actingBy,
    }, { where: { id: managerInfo.id }, transaction }));
    const createManagerPromises = managersInfo
      .filter((managerInfo) => !managerInfo.id)
      .map((managerInfo) => Managers.create({ ...managerInfo, PartnerId: id }, { transaction }));

    return [...deleteManagerPromises, ...updateManagerPromises, ...createManagerPromises];
  }

  updatePartnersBanksInfo(updatedPartner, PartnerId, transaction) {
    const { deletedBanks, updatedBanks, banksInfo } = updatedPartner;
    const deleteBanksPromises = deletedBanks
      .map((deletedBank) => PartnerBanks.destroy({ where: { id: deletedBank.id }, transaction }));
    const updateBanksPromises = updatedBanks
      .map((bankInfo) => PartnerBanks.update({
        name: bankInfo.name,
        IBAN: bankInfo.IBAN,
      }, { where: { id: bankInfo.id }, transaction }));

    const createBanksPromises = banksInfo
      .filter((bankInfo) => !bankInfo.id)
      .map((bankInfo) => PartnerBanks.create({ ...bankInfo, PartnerId }, { transaction }));

    return [...deleteBanksPromises, updateBanksPromises, createBanksPromises];
  }

  async updatePartner(updatedPartner, id) {
    let transaction;
    try {
      transaction = await sequeilize.transaction();
      const partnerData = await Partner.update({ ...updatedPartner }, {
        where: { id },
        transaction,
      });
      await Promise
        .all([
          ...this.updatePartnerManagers(updatedPartner, id, transaction),
          ...this.updatePartnersBanksInfo(updatedPartner, id, transaction),
        ]);
      await transaction.commit();

      return partnerData;
    } catch (e) {
      if (transaction) {
        await transaction.rollback();
      }
      throw e;
    }
  }

  async removePartner(id) {
    let transaction;
    try {
      transaction = await sequeilize.transaction();
      const partnerData = await Partner.findOne({ where: { id }, transaction });
      const removed = await partnerData.destroy();
      await transaction.commit();
      return removed;
    } catch (e) {
      if (transaction) {
        await transaction.rollback();
      }
      throw e;
    }
  }

  async getPartner(id) {
    const partner = await Partner
      .findByPk(id, {
        include: [
          { model: PartnerBanks, as: 'banksInfo' },
          { model: Managers, as: 'managersInfo' },
        ],
      });
    return partner;
  }

  async getAllPartners(userId, limit, offset, search) {
    const whereClause = {
      userId,
    };

    if (search) {
      whereClause.name = {
        [Op.or]: [
          {
            name: {
              [Op.like]: `%${search}%`,
            },
          },
          {
            partnerId: {
              [Op.like]: `%${search}%`,
            },
          },
          {
            partnerTaxNumber: {
              [Op.like]: `%${search}%`,
            },
          },
        ],
      };
    }
    const allPartners = await Partner.findAndCountAll({
      where: whereClause,
      limit,
      offset,
    });
    return allPartners;
  }
}

export default new PartnerService();
