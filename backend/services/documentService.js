import { Op } from 'sequelize';
import { Contract, Partner, CompletedWorkAct } from '../models/index.js';

class DocumentService {
  async addContract(contract, UserId) {
    const newContract = await Contract.create({ ...contract, UserId });
    return newContract;
  }

  async updateContract(updatedContract, id) {
    const contractData = await Contract.update({ ...updatedContract }, { where: { id } });
    return contractData;
  }

  async removeContract(id) {
    const contractData = await Contract.findOne({ where: { id } });
    const removed = await contractData.destroy();
    return removed;
  }

  async getContract(id) {
    const contract = await Contract.findByPk(id);
    const partner = await Partner.findByPk(contract.PartnerId);
    return {
      contract,
      partnerName: partner.name,
    };
  }

  async addCompletedWorkAct(contract, UserId) {
    const newCompletedWorkAct = await CompletedWorkAct.create({ ...contract, UserId });
    return newCompletedWorkAct;
  }

  async updateCompletedWorkAct(updatedCompletedWorkAct, id) {
    const completedWorkActData = await CompletedWorkAct.update({ ...updatedCompletedWorkAct }, { where: { id } });
    return completedWorkActData;
  }

  async removeCompletedWorkAct(id) {
    const completedWorkActData = await CompletedWorkAct.findOne({ where: { id } });
    const removed = await completedWorkActData.destroy();
    return removed;
  }

  async getCompletedWorkAct(id) {
    const completedWorkAct = await CompletedWorkAct.findByPk(id);
    const partner = await Partner.findByPk(completedWorkAct.PartnerId);
    const contract = await Contract.findByPk(completedWorkAct.ContractId);
    return {
      completedWorkAct,
      contractName: contract.name,
      partnerName: partner.name,
    };
  }

  async getAllDocuments(userId, limit, offset, order, search, startDate, endDate) {
    const whereClause = {
      UserId: userId,
    };

    if (search) {
      whereClause.name = {
        [Op.like]: `%${search}%`,
      };
    }

    if (startDate && endDate) {
      whereClause.dateOfSign = {
        [Op.between]: [startDate, endDate],
      };
    }

    const contractPromise = await Contract.findAndCountAll({
      where: whereClause,
      limit,
      offset,
      order: order ? [['contractAmount', order]] : undefined,
    });

    const completedWorkActPromise = await CompletedWorkAct.findAndCountAll({
      where: whereClause,
      limit,
      offset,
      order: order ? [['completedWorkActAmount', order]] : undefined,
    });

    const partnerPromise = await Partner.findAll({
      where: {
        UserId: userId,
      },
    });
    const [contracts, completedWorkAct, partners] = await Promise
      .all([contractPromise, completedWorkActPromise, partnerPromise]);
    return {
      contracts,
      completedWorkAct,
      partners,
    };
  }

  async getContractsByPartnerId(id) {
    const contracts = await Contract.findAll({
      where: {
        PartnerId: id,
      },
    });
    return contracts;
  }
}

export default new DocumentService();
