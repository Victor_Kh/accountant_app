import User from './userModel.js';
import tokenModel from './tokenModel.js';
import Partner from './partnerModel.js';
import Contract from './contractModel.js';
import CompletedWorkAct from './completedWorkActModel.js';
import Managers from './managersModel.js';
import PartnerBanks from './banksModel.js';

User.hasOne(tokenModel, { onDelete: 'cascade' });
tokenModel.belongsTo(User, { foreignKey: 'UserId' });
User.hasMany(Contract, { onDelete: 'cascade' });
User.hasMany(CompletedWorkAct, { onDelete: 'cascade' });
User.hasMany(Partner, { onDelete: 'cascade' });
Partner.hasMany(Contract, { onDelete: 'cascade' });
Partner.hasMany(CompletedWorkAct, { onDelete: 'cascade' });
Partner.hasMany(Managers, { onDelete: 'cascade', onUpdate: 'cascade', as: 'managersInfo' });
Partner.hasMany(PartnerBanks, { onDelete: 'cascade', as: 'banksInfo' });
Partner.belongsTo(User, { foreignKey: 'UserId' });
Managers.belongsTo(Partner, { foreignKey: 'PartnerId', onUpdate: 'cascade' });
Contract.hasMany(CompletedWorkAct, { onDelete: 'cascade' });
Contract.belongsTo(Partner, { foreignKey: 'PartnerId' });
Contract.belongsTo(User, { foreignKey: 'UserId' });
CompletedWorkAct.belongsTo(Partner, { foreignKey: 'PartnerId' });
CompletedWorkAct.belongsTo(User, { foreignKey: 'UserId' });
PartnerBanks.belongsTo(Partner, { foreignKey: 'PartnerId' });

export {
  User, tokenModel, Partner, Contract, CompletedWorkAct, Managers, PartnerBanks,
};
