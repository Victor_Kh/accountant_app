import Sequelize from 'sequelize';
import sequelize from '../utils/database.js';

const managers = sequelize.define('Managers', {
  name: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  position: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  actingBy: {
    type: Sequelize.STRING,
    allowNull: false,
  },
});

export default managers;
