import Sequelize from 'sequelize';
import sequelize from '../utils/database.js';

const contract = sequelize.define('Contract', {
  name: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  dateOfSign: {
    type: Sequelize.DATE,
    allowNull: false,
  },
  dateOfEnd: {
    type: Sequelize.DATE,
    allowNull: false,
  },
  contractAmount: {
    type: Sequelize.INTEGER,
    allowNull: false,
  },
  currencyOfContract: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  isActive: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: true,
  },
});

export default contract;
