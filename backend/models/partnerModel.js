import Sequelize from 'sequelize';
import sequelize from '../utils/database.js';

const partner = sequelize.define('Partner', {
  name: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  partnerId: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  partnerTaxNumber: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  email: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  phone: {
    type: Sequelize.STRING,
    allowNull: true,
  },
  registrationAddress: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  actualAddress: {
    type: Sequelize.STRING,
    allowNull: false,
  },
});

export default partner;
