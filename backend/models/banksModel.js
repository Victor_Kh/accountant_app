import Sequelize from 'sequelize';
import sequelize from '../utils/database.js';

const PartnerBanks  = sequelize.define('PartnerBanks', {
	name: {
		type: Sequelize.STRING,
		allowNull: false,
	},
	IBAN: {
		type: Sequelize.STRING,
		allowNull: false,
	},
});

export default PartnerBanks;
