class ApiError extends Error {
	status;
	errors;
	constructor(status, message, errors) {
		super(message);
		this.status = status;
		this.errors = errors
	}

	static UnautorizedError() {
		return new ApiError(401, 'Користувач не увійшов до системи, увійдіть, будь-ласка', [])
	}

	static BadCredentials(message, errors) {
		return new ApiError(400, message, errors = [])
	}

	static UserAlreadyExist(message, errors) {
		return new ApiError(400, message, errors = [])
	}

}

export default ApiError
