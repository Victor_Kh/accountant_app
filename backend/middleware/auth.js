import tokenService from '../services/tokenService.js';
import ApiError from '../exeption/exeption.js';

const authMiddleware = (req, res, next) => {
  try {
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      return next(ApiError.UnautorizedError());
    }
    const accessToken = authHeader.split(' ')[1];
    if (!accessToken) {
      return next(ApiError.UnautorizedError());
    }
    const userData = tokenService.verifyAccessToken(accessToken);
    if (!userData) {
      return next(ApiError.UnautorizedError());
    }
    req.user = userData;
    next();
  } catch (e) {
    next(ApiError.UnautorizedError());
  }
};

export default authMiddleware;
