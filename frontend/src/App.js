import { RouterProvider } from 'react-router-dom';
import { observer } from 'mobx-react-lite';
import './App.scss';
import { useContext, useEffect } from 'react';
import Context from './index';
import router from './pages/routes';

function App() {
  	const { userStore } = useContext(Context);
  	useEffect(() => {
		if (localStorage.getItem('accessToken')) {
			userStore.checkAuth();
		}
	}, []);
	return (
		<RouterProvider router={router} />
	);
}

export default observer(App);
