import React from 'react';
import { useAsyncValue } from 'react-router-dom';
import { format } from 'date-fns';
import { uk } from 'date-fns/locale';

const Document = ({ type }) => {
	const document = useAsyncValue();
	let singleDocument;
	let	contractName;
	let	partnerName;
	if (type === 'Контракт') {
		singleDocument = document.contract;
		partnerName = document.partnerName;
	}
	if (type === 'Акт') {
		singleDocument = document.completedWorkAct;
		partnerName = document.partnerName;
		contractName = document.contractName;
	}
	return (
		<div className='document'>
			<h3 className='document__title'>
			    {type} {singleDocument.name} від {format(new Date(singleDocument.dateOfSign), 'PPP', { locale: uk })}
			</h3>
		  	<div className="table-row">
			  	<div className="table-cell">Контрагент</div>
			  	<div className="table-cell">{partnerName}</div>
			</div>
		  	<div className="table-row">
			  	<div className="table-cell">Сума {type === 'Акт' ? 'акту' : 'контракту'}</div>
			  	<div className="table-cell">{singleDocument.contractAmount || singleDocument.completedWorkActAmount}</div>
			</div>
		  	<div className="table-row">
			  	<div className="table-cell">Валюта {type === 'Акт' ? 'акту' : 'контракту'}</div>
			  	<div className="table-cell">{singleDocument.currencyOfContract || singleDocument.currencyOfCompletedWorkAct}</div>
			</div>
		  	<div className="table-row">
			  	<div className="table-cell">Статус виконання</div>
			  	<div className="table-cell">{singleDocument.isActive ? 'V' : 'X'}</div>
			</div>
			{type === 'Акт'
				? <div className="table-row">
					<div className="table-cell">Контракт</div>
					<div className="table-cell">{contractName}</div>
				</div>
				: null}
			<div className="table-row">
				<div className="table-cell">Створено</div>
				<div className="table-cell">{format(new Date(singleDocument.createdAt), 'PPP', { locale: uk })}</div>
			</div>
		  	<div className="table-row">
				<div className="table-cell">Змінено</div>
				<div className="table-cell">{format(new Date(singleDocument.updatedAt), 'PPP', { locale: uk })}</div>
		  	</div>
		</div>
	);
};

export default Document;
