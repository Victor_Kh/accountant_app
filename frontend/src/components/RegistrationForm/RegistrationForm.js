import React, { useContext } from 'react';
import { observer } from 'mobx-react-lite';
import { Formik, Form } from 'formik';
import Context from '../../index';
import Input from '../common/input/Input';
import Button from '../common/button/Button';
import formsValidationSchema from '../../formsValidationSchema/formsValidationSchema';

const RegistrationForm = () => {
	const { userStore } = useContext(Context);

	const registrationFormSubmitHandler = (values) => {
		const { name, email, password } = values;
		userStore.registration(name, email, password);
	};

	const initialValue = {
		name: '',
		email: '',
		password: '',
		confirmedPassword: '',
	};

	const registrationFormValidationSchema = formsValidationSchema('registrationForm');

	return (
		<Formik
			initialValues={initialValue}
			validationSchema={registrationFormValidationSchema}
			onSubmit={registrationFormSubmitHandler}
		>
			<div className='form-holder login-form'>
				<Form>
					<input type="hidden" value='p' name='isregistration'/>
					<div className='form-row'>
						<Input id='name' label="Ім'я" name='name' type='text' />
					</div>
					<div className='form-row'>
						<Input id='email' label='Електронна пошта' name='email' type='text' />
					</div>
					<div className='form-row'>
						<Input id='password' label='Пароль' name='password' type='password' />
					</div>
					<div className='form-row'>
						<Input id='confirmedPassword' label='Повторіть пароль' name='confirmedPassword' type='password' />
					</div>
					<div className='form-row button-row'>
						<Button type='submit'>Зареєструватися</Button>
					</div>
				</Form>
			</div>
		</Formik>
	);
};

export default observer(RegistrationForm);
