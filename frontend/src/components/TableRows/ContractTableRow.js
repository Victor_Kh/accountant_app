import React from 'react';
import { Link } from 'react-router-dom';
import { format } from 'date-fns';
import { uk } from 'date-fns/locale';

const ContractTableRow = ({ contract, partners, order }) => {
	const partner = partners.find((partner) => partner.id === contract.PartnerId);
	return (
		<Link to={`/contract/${contract.id}`} className='table-row'>
			<div className='table-cell'>{order}</div>
			<div className='table-cell'>{contract.name}</div>
			<div className='table-cell'>{partner.name}</div>
			<div className='table-cell'>{format(new Date(contract.dateOfSign), 'PPP', { locale: uk })}</div>
			<div className='table-cell'>{format(new Date(contract.dateOfEnd), 'PPP', { locale: uk })}</div>
			<div className='table-cell'>{contract.contractAmount}</div>
			<div className='table-cell'>{contract.currencyOfContract}</div>
			<div className='table-cell'>{contract.isActive ? 'V' : 'X'}</div>
		</Link>
	);
};

export default ContractTableRow;
