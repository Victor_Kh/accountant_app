import React from 'react';
import { format } from 'date-fns';
import { uk } from 'date-fns/locale';
import { Link } from 'react-router-dom';

const ActOfCompleteWorkRow = ({ actOfCompletedWorks, partners, order }) => {
	const partner = partners.find((partner) => partner.id === actOfCompletedWorks.PartnerId);
	return (
		<Link to={`/act/${actOfCompletedWorks.id}`} className='table-row'>
			<div className='table-cell'>{order}</div>
			<div className='table-cell'>{actOfCompletedWorks.name}</div>
			<div className='table-cell'>{partner.name}</div>
			<div className='table-cell'>{format(new Date(actOfCompletedWorks.dateOfSign), 'PPP', { locale: uk })}</div>
			<div className='table-cell'>{actOfCompletedWorks.completedWorkActAmount}</div>
			<div className='table-cell'>{actOfCompletedWorks.currencyOfCompletedWorkAct}</div>
			<div className='table-cell'>{actOfCompletedWorks.isActive ? 'V' : 'X'}</div>
		</Link>
	);
};

export default ActOfCompleteWorkRow;
