import React from 'react';
import { Link } from 'react-router-dom';

const PartnerTableRow = ({ partner, order }) => {
	return (
		<Link to={`/partner/${partner.id}`} className='table-row'>
			<div className='table-cell'>{order}</div>
			<div className='table-cell'>{partner.name}</div>
			<div className='table-cell'>{partner.partnerId}</div>
			<div className='table-cell'>{partner.partnerTaxNumber}</div>
			<div className='table-cell'>{partner.email}</div>
			<div className='table-cell'>{partner.phone}</div>
		</Link>
	);
};

export default PartnerTableRow;
