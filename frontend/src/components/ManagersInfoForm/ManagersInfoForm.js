import React from 'react';
import { ErrorMessage, FieldArray } from 'formik';
import Input from '../common/input/Input';
import deleteBtn from '../../assets/delete-button.svg';
import addBtn from '../../assets/add-button.svg';

const ManagersInfoForm = ({ values }) => {
	return (
		<FieldArray
			name='managersInfo'
			render={(arrayHelpers) => (
				<>
					<div className='magagersInfoForm'>
						{ values.managersInfo.map((manager, index) => (
							<div className="managersInfoRow" key={index}>
								<div className="form-row">
									<Input type="text"
										   name={`managersInfo[${index}].position`}
										   label='Позиція відповідального'
									/>
									<ErrorMessage name={`managersInfo[${index}].position`}></ErrorMessage>
								</div>
								<div className="form-row">
									<Input type="text"
										   name={`managersInfo[${index}].name`}
										   label='П.І.Б. відповідального'
									/>
									<ErrorMessage name={`managersInfo[${index}].name`}></ErrorMessage>
								</div>
								<div className="form-row">
									<Input type="text"
										   name={`managersInfo[${index}].actingBy`}
										   label='Діє на підставі'
									/>
									<ErrorMessage name={`managersInfo[${index}].actingBy`}></ErrorMessage>
								</div>
								{values.managersInfo.length > 1
									? <span
										className='form-row-btn form-row-btn--delete'
										onClick={() => {
											values.managersInfo[index].id && values.deletedManagers.push(values.managersInfo[index]);
											arrayHelpers.remove(index);
										}}
									>
										<img src={deleteBtn} alt="deleteIcon"/>
									</span>
									: null}
							</div>
						)) }
					</div>
					<span
						className='form-row-btn'
						onClick={() => arrayHelpers.push({ name: '', actingBy: '', position: '' })}
					>
						<img src={addBtn} alt="addIcon"/>
					</span>
				</>
			)}
		/>
	);
};

export default ManagersInfoForm;
