import React from 'react';
import { useAsyncValue } from 'react-router-dom';

const Partner = () => {
	const singlePartner = useAsyncValue();
	return (
		<div className='document'>
			<div className="table-row">
				<div className="table-cell">Контрагент</div>
				<div className="table-cell">{singlePartner.name}</div>
			</div>
			<div className="table-row">
				<div className="table-cell">Код ЄДРПОУ</div>
				<div className="table-cell">{singlePartner.partnerId}</div>
			</div>
			<div className="table-row">
				<div className="table-cell">ІПН</div>
				<div className="table-cell">{singlePartner.partnerTaxNumber}</div>
			</div>
			<div className="table-row">
				<div className="table-cell">Електронна адреса</div>
				<div className="table-cell">{singlePartner.email}</div>
			</div>
			<div className="table-row">
				<div className="table-cell">Контактний телефон</div>
				<div className="table-cell">{singlePartner.phone}</div>
			</div>
		</div>
	);
};

export default Partner;
