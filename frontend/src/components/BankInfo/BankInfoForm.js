import React from 'react';
import { ErrorMessage, FieldArray } from 'formik';
import Input from '../common/input/Input';
import deleteBtn from '../../assets/delete-button.svg';
import addBtn from '../../assets/add-button.svg';

const BankInfoForm = ({ values }) => {
	return (
		<FieldArray
			name='banksInfo'
			render={(arrayHelpers) => (
				<>
					<div className='bankInfoForm'>
						{ values.banksInfo.map((bank, index) => (
							<div className="bankInfoRow" key={index}>
								<div className="form-row">
									<Input type="text"
										   name={`banksInfo[${index}].IBAN`}
										   label='IBAN'
									/>
									<ErrorMessage name={`banksInfo[${index}].IBAN`}></ErrorMessage>
								</div>
								<div className="form-row">
									<Input type="text"
										   name={`banksInfo[${index}].name`}
										   label='Назва банку'
									/>
									<ErrorMessage name={`banksInfo[${index}].name`}></ErrorMessage>
								</div>
								{values.banksInfo.length > 1
									? <span
										className='form-row-btn form-row-btn--delete'
										onClick={() => {
											values.banksInfo[index].id && values.deletedBanks.push(values.banksInfo[index]);
											arrayHelpers.remove(index);
										}}
									>
										<img src={deleteBtn} alt="deleteIcon"/>
									</span>
									: null}
							</div>
						)) }
					</div>
					<span
						className='form-row-btn'
						onClick={() => arrayHelpers.push({ name: '', IBAN: '' })}
					>
						<img src={addBtn} alt="addIcon"/>
					</span>
				</>
			)}
		/>
	);
};

export default BankInfoForm;
