import React, { useContext } from 'react';
import { observer } from 'mobx-react-lite';
import { Formik, Form } from 'formik';
import Context from '../../index';
import formsValidationSchema from '../../formsValidationSchema/formsValidationSchema';
import Input from '../common/input/Input';
import Button from '../common/button/Button';

const LoginForm = () => {
	const { userStore } = useContext(Context);

	const initialValues = {
		email: '',
		password: '',
	};

	const loginFormSubmitHandler = (values) => {
		const { email, password } = values;
		userStore.login(email, password);
	};

	const loginFormValidationSchema = formsValidationSchema('loginForm');

	return (
		<Formik
			initialValues={initialValues}
			validationSchema={loginFormValidationSchema}
			onSubmit={loginFormSubmitHandler}
		>
			<div className='form-holder login-form'>
				<Form>
					<div className='form-row'>
						<Input id='email'
							   label='Електронна пошта'
							   name='email'
							   type='text'
						/>
					</div>
					<div className='form-row'>
						<Input id='password'
							   name='password'
							   label='Пароль'
							   type='password'/>
					</div>
					<div className='form-row button-row'>
						<Button type='submit'>Увійти</Button>
					</div>
				</Form>
			</div>
		</Formik>
	);
};

export default observer(LoginForm);
