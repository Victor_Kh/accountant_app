import React, { useContext, useEffect } from 'react';
import { useAsyncValue } from 'react-router-dom';
import PartnerTableRow from '../TableRows/PartnerTableRow';
import Context from '../../index';

const PartnersTable = () => {
	const { partnersFilterStore } = useContext(Context);
	const allPartnersData = useAsyncValue();
	const { rows: allPartners, count } = allPartnersData;
	const activePage = partnersFilterStore.page;
	const limitPerPage = partnersFilterStore.limit;
	useEffect(() => {
		partnersFilterStore.setTotalCount(count);
	}, [count]);
	return (
		<div className='table'>
			<div className='table-header'>Партнери</div>
			<div className='table-caption table-row'>
				<div className='table-cell'>№</div>
				<div className='table-cell'>Контрагент</div>
				<div className='table-cell'>Код ЄДРПОУ</div>
				<div className='table-cell'>ІПН</div>
				<div className='table-cell'>Електронна пошта</div>
				<div className='table-cell'>Контактний телефон</div>
			</div>
			{allPartners.map((partner, idx) => (
				<PartnerTableRow
					partner={partner}
					order={(activePage * limitPerPage) + idx + 1 - limitPerPage}
					key={partner.id}
				/>))}
		</div>
	);
};

export default PartnersTable;
