import React, { useContext, useEffect } from 'react';
import { useAsyncValue } from 'react-router-dom';
import { observer } from 'mobx-react-lite';
import ActOfCompleteWorkRow from '../TableRows/ActOfCompleteWorkRow';
import ContractTableRow from '../TableRows/ContractTableRow';
import Context from '../../index';

const DocumentsTable = ({ isContractsVisible }) => {
  	const documentsData = useAsyncValue();
	const { data: { contracts, completedWorkAct, partners } } = documentsData;
	const { documentsFilterStore, globalStore } = useContext(Context);
	const count = contracts.count || completedWorkAct.count;
	const activePage = documentsFilterStore.page;
	const limitPerPage = documentsFilterStore.limit;
	globalStore.setAllDocuments({ contracts, completedWorkAct });
	globalStore.setAllPartners(partners);
	useEffect(() => {
		documentsFilterStore.setTotalCount(count);
	}, [count]);
	const createTableCaption = (isContractsVisible) => {
		if (isContractsVisible) {
			return (
				<div className='table-caption table-row'>
					<div className='table-cell'>№</div>
					<div className='table-cell'>Контракт</div>
					<div className='table-cell'>Контрагент</div>
					<div className='table-cell'>Дата підписання</div>
					<div className='table-cell'>Дата закінчення</div>
					<div className='table-cell'>Сумма контракту</div>
					<div className='table-cell'>Валюта контракту</div>
					<div className='table-cell'>Виконання</div>
				</div>
			);
		}
		return (
			<div className='table-caption table-row'>
				<div className='table-cell'>№</div>
				<div className='table-cell'>Акт</div>
				<div className='table-cell'>Контрагент</div>
				<div className='table-cell'>Дата підписання</div>
				<div className='table-cell'>Сумма акту</div>
				<div className='table-cell'>Валюта акту</div>
				<div className='table-cell'>Виконання</div>
			</div>
	  );
	};
	const createTable = (documents, partners) => {
		const { contracts, completedWorkAct } = documents;
		if (contracts && isContractsVisible) {
			return contracts.rows.map((contract, idx) => (
				<ContractTableRow
					contract={contract}
					partners={partners}
					order={(activePage * limitPerPage) + idx + 1 - limitPerPage}
					key={contract.id}
				/>
			));
		}
		if (completedWorkAct && !isContractsVisible) {
			return completedWorkAct.rows.map((actOfCompletedWork, idx) => (
				<ActOfCompleteWorkRow
					actOfCompletedWorks={actOfCompletedWork}
					partners={partners}
					order={(activePage * limitPerPage) + idx + 1 - limitPerPage}
					key={actOfCompletedWork.id}
				/>
			));
		}
	};
	return (
		<div className='table'>
			<div className='table-header'>
				  {isContractsVisible ? 'Контракти' : 'Акти виконаних робіт'}
			</div>
		  	{createTableCaption(isContractsVisible)}
			{createTable({ contracts, completedWorkAct }, partners)}
		</div>
	);
};

export default observer(DocumentsTable);
