import React from 'react';
import { useField } from 'formik';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import './datepicker.scss';

const DatePickerField = ({
	name = '', label, id, ...props
}) => {
	const [field, meta, helpers] = useField(name);

	const { value } = meta;
	const { setValue } = helpers;

	return (
		<div className='inputWrapper'>
			<label>
				{label}
				<DatePicker
					{...field}
					selected={value}
					onChange={(date) => setValue(date)}
					autoComplete='off'
					dateFormat='dd/MM/yyyy'
					onKeyDown={(e) => {
						e.preventDefault();
					}}
					{...props}
				/>
			</label>
			{meta.touched && meta.error ? (
				<div className="error">{meta.error}</div>
			) : null}
		</div>
	);
};

export default DatePickerField;
