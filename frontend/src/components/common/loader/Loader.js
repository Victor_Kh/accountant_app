import React from 'react';

const Loader = () => {
	return (
		<div id="custom__ripple_Loader" className="box">
		  	<div className="ripple__rounds"></div>
		</div>
	);
};

export default Loader;
