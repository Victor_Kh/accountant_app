import React from 'react';
import { useField } from 'formik';

const Input = ({ label, ...props }) => {
	const [field, meta] = useField(props);
	return (
		<div className='inputWrapper'>
			<label>
				{label}
				<input className="input" {...field} {...props} />
			</label>
			{meta.touched && meta.error ? (
				<div className="error">{meta.error}</div>
			) : null}
		</div>
	);
};

export default Input;
