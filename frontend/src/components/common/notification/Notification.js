import React, { useContext, useEffect } from 'react';
import Context from '../../../index';
import classNames from '../../../utils/helpers';

const Notification = ({ message, setIsNotificationShown, isError }) => {
	const { userStore } = useContext(Context);
	useEffect(() => {
		setTimeout(() => {
			setIsNotificationShown(false);
			userStore.setNotificationState({ name: '', action: '' });
		}, 3000);
	}, []);
	return (
		<div className={classNames(['notification', isError ? 'error' : ''])}>
			{message}
		</div>
	);
};

export default Notification;
