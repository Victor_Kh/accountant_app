import React from 'react';
import { useNavigate } from 'react-router-dom';

const Modal = ({ active, setActive, children }) => {
	const navigate = useNavigate();
	const modalHandler = () => {
		navigate(-1);
	  	setActive && setActive(false);
	};
	return (
		<div className={active ? 'modal active' : 'modal'} onClick={modalHandler}>
		  	<div className="modal__content" onClick={(e) => e.stopPropagation()}>
			  	{children}
			</div>
		</div>
	);
};

export default Modal;
