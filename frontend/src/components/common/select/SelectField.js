import React from 'react';
import Select from 'react-select';
import { useField } from 'formik';
import './selectfield.scss';

function SelectField({ label, options, ...props }) {
	const [field, meta] = useField(props.name);
	const defaultValue = options.find((option) => option.value === field.value);
	const handleChange = (selectedOption) => {
		field.onChange({ target: { name: field.name, value: selectedOption.value } });
	};

	return (
		<div className="inputWrapper">
			<label>
				{label}
				<Select
					{...field}
					{...props}
					options={options}
					value={defaultValue}
					onChange={handleChange}
					className='select-field'
					styles={{
						control: (base) => ({
							...base,
							boxShadow: 'none',
						}),
					}}
				/>
			</label>
			{meta.touched && meta.error ? (
				<div className="error">{meta.error}</div>
			) : null}
		</div>
	);
}

export default SelectField;
