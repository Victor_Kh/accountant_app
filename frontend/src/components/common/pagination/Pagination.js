import React from 'react';
import { observer } from 'mobx-react-lite';
import classNames from '../../../utils/helpers';

const Pagination = ({ store }) => {
	const pageCount = Math.ceil(store.totalCount / store.limit);
	const pages = [];
	for (let i = 0; i < pageCount; i += 1) {
		pages.push(i + 1);
	}
	return (
		<div className='pagination'>
			{pages.map((page) => (
				<span
					key={page}
					className={classNames(['pagination-item', page === +store.page ? 'active' : ''])}
					onClick={() => store.setPage(page)}
				>
					{page}
				</span>
			))}
		</div>
	);
};

export default observer(Pagination);
