import React, { useContext } from 'react';
import { observer } from 'mobx-react-lite';
import DatePicker from 'react-datepicker';
import Select from 'react-select';
import { compareAsc, formatISO, parseISO } from 'date-fns';
import Context from '../../../index';
import Button from '../button/Button';
import { resetFilter } from '../../../utils/helpers';

const DocumentsFilter = () => {
	const {
		documentsFilterStore: {
			order, setOrder, startDate, endDate, setStartDate, setEndDate, dateError, setDateError,
		},
	} = useContext(Context);
	const { documentsFilterStore } = useContext(Context);

	return (
		<div className='filters'>
			<div className="form-row">
				<label className='filter-label'>
					Сортувати
					<Select
						value={order === 'DESC'
							? { value: 'DESC', label: 'за зменшенням' }
							: order && { value: 'ASC', label: 'за збільшенням' }}
						options={[{ value: 'DESC', label: 'за зменшенням' }, { value: 'ASC', label: 'за збільшенням' }]}
						name='orderQuery'
						onChange={(selectedOption) => {
							setOrder(selectedOption.value);
						}}
					/>
				</label>
			</div>
			<div className="form-row">
				<label className='filter-label'>
					Шукати з
					<DatePicker
						selected={startDate ? parseISO(startDate) : null}
						onChange={(date) => {
							setStartDate(formatISO(date));
							setDateError(compareAsc(date, parseISO(endDate)) === 1);
						}}
						onKeyDown={(e) => {
							e.preventDefault();
						}}
						dateFormat='dd/MM/yyyy'
					/>
				</label>
			</div>
			<div className="form-row">
				<label className='filter-label'>
					Шукати по
					<DatePicker
						selected={endDate ? parseISO(endDate) : null}
						onChange={(date) => {
							setEndDate(formatISO(date));
							setDateError(compareAsc(date, parseISO(startDate)) === -1);
						}}
						onKeyDown={(e) => {
							e.preventDefault();
						}}
						dateFormat='dd/MM/yyyy'
					/>
				</label>
				{dateError && <div className='error'>Ця дата не може бути менше за "Шукати з"</div>}
			</div>
			<Button onclick={() => resetFilter(documentsFilterStore)}>Скинути фільтр</Button>
		</div>
	);
};

export default observer(DocumentsFilter);
