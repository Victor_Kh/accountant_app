import React from 'react';
import { observer } from 'mobx-react-lite';
import Select from 'react-select';

const Filter = ({ store, render }) => {
	const {
		limit, setLimit, searchValue, setSearchValue,
	} = store;

	return (
		<div className='filters'>
			<div className="form-row">
				<label className='filter-label'>
					Показувати по
					<Select
						value={{ value: limit, label: limit }}
						options={[{ value: 10, label: 10 }, { value: 25, label: 25 }, { value: 50, label: 50 }]}
						defaultValue={{ value: 10, label: 10 }}
						onChange={(selectedOption) => {
							setLimit(selectedOption.value);
						}}
					/>
				</label>
			</div>
			<div className="form-row">
				<div className='inputWrapper'>
					<label className='filter-label'>
						Пошук
						<input
							className='input'
							value={searchValue}
							onChange={(e) => {
								setSearchValue(e.target.value);
							}}
						/>
					</label>
				</div>
			</div>
			{render}
		</div>
	);
};

export default observer(Filter);
