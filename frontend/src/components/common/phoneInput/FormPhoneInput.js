import React from 'react';
import { useField } from 'formik';
import PhoneInput from 'react-phone-number-input';
import 'react-phone-number-input/style.css';
import './phoneinput.scss';

const FormPhoneInput = ({ label, ...props }) => {
	const [field, meta, helpers] = useField(props);
	return (
		<div className='inputWrapper'>
			<label>
				{label}
				<PhoneInput
					{...props}
					{...field}
					value={field.value}
					defaultCountry='UA'
					onChange={(value) => {
						helpers.setValue(value);
					}}
				/>
			</label>
			{meta.touched && meta.error ? (
				<div className="error">{meta.error}</div>
			) : null}
		</div>
	);
};

export default FormPhoneInput;
