import React from 'react';

const Footer = () => {
	return (
		<footer className='footer'>
			<div className="container">
				<span className='copywrite'>© VD</span>
			</div>
		</footer>
	);
};

export default Footer;
