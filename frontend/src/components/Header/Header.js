import React, { useContext } from 'react';
import { NavLink, useNavigate, useLocation } from 'react-router-dom';
import { observer } from 'mobx-react-lite';
import Context from '../../index';
import Button from '../common/button/Button';
import logo from '../../assets/logo.png';

const Header = () => {
	const { userStore } = useContext(Context);
	const navigate = useNavigate();
	const location = useLocation();
	const logoutBtnHandler = () => {
		navigate('/');
		userStore.logout();
	};
	return (
		<header className='header'>
			<div className="container ">
				<div className="logo">
					<NavLink to={'/'}>
					  <img src={logo} alt="logo" className="logo-img"/>
					</NavLink>
				</div>
				<div className="nav-wrapper">
					<nav className="nav">
						<ul className='nav-list'>
							<li className="nav-list__item"><NavLink to={'/'}>Головна</NavLink></li>
						  	{!userStore.isAuth && <li className="nav-list__item"><NavLink to={'/login'}>Увійти</NavLink></li>}
							{userStore.isAuth && <li className="nav-list__item">{
								location.pathname === '/partners'
									? <NavLink to={'/partners'} onClick={(e) => e.preventDefault()}>Партнери</NavLink>
									: <NavLink to={'/partners'}>Партнери</NavLink>
							}</li>}
							{userStore.isAuth && <li className="nav-list__item">{
								location.pathname === '/documents'
									? <NavLink to={'/documents'} onClick={(e) => e.preventDefault()}>Документи</NavLink>
									: <NavLink to={'/documents'}>Документи</NavLink>
							}</li>}
						</ul>
					</nav>
				  	{userStore.isAuth && <Button type='submit' onclick={() => logoutBtnHandler()}>Logout</Button>}
				</div>
			</div>
		</header>
	);
};

export default observer(Header);
