import { makeAutoObservable } from 'mobx';

export default class DocumentsFilterStore {
	page = 1;

	limit = 10;

	totalCount = 0;

	searchValue = '';

	order = '';

	startDate = null;

	endDate = null;

	dateError = false;

	constructor() {
		makeAutoObservable(this, {}, { autoBind: true });
	}

	setPage(page) {
		this.page = page;
	}

	setLimit(limit) {
		this.limit = limit;
	}

	setSearchValue(searchValue) {
		this.searchValue = searchValue;
	}

	setTotalCount(totalCount) {
		this.totalCount = totalCount;
	}

	setStartDate(date) {
		this.startDate = date;
	}

	setEndDate(date) {
		this.endDate = date;
	}

	setOrder(order) {
		this.order = order;
	}

	setDateError(isError) {
		this.dateError = isError;
	}
}
