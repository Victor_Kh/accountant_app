import { makeAutoObservable } from 'mobx';
import DocumentService from '../services/documentService';

export default class GlobalStore {
	isSwitcherFirstTabVisible = true;

	allDocuments = {};

	allPartners = [];

	contractsByPartnersId = [];

	constructor() {
		makeAutoObservable(this, {}, { autoBind: true });
	}

	setSwitcherFirstTabVisible(isSwitcherFirstTabVisible) {
		this.isSwitcherFirstTabVisible = isSwitcherFirstTabVisible;
	}

	setAllDocuments(allDocuments) {
		this.allDocuments = allDocuments;
	}

	setAllPartners(allPartners) {
		this.allPartners = allPartners;
	}

	setContactsByPartnersId(contracts) {
		this.contractsByPartnersId = contracts;
	}

	async getContractsByPartnersId(partnerId) {
		try {
			const response = await DocumentService.getContractsByPartnersId(partnerId);
			this.setContactsByPartnersId(response.data);
		}
		catch (e) {
			throw new Error(e);
		}
	}
}
