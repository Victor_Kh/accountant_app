import { makeAutoObservable } from 'mobx';

export default class PartnersFilterStore {
	page = 1;

	limit = 10;

	totalCount = 0;

	searchValue = '';

	constructor() {
		makeAutoObservable(this, {}, { autoBind: true });
	}

	setPage(page) {
		this.page = page;
	}

	setLimit(limit) {
		this.limit = limit;
	}

	setSearchValue(searchValue) {
		this.searchValue = searchValue;
	}

	setTotalCount(totalCount) {
		this.totalCount = totalCount;
	}
}
