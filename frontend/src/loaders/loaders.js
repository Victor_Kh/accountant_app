import { defer } from 'react-router-dom';
import DocumentService from '../services/documentService';
import PartnerService from '../services/partnerService';

const getAllDocumentsData = async (page, limit, search, order, startDate, endDate) => {
	return await DocumentService.getAllDocuments(page, limit, search, order, startDate, endDate);
};
const allDocumentLoader = async ({ request }) => {
	const searchParams = request.url.split('?')[1];
	const pageParam = new URLSearchParams(searchParams).get('page');
	const limitParam = new URLSearchParams(searchParams).get('limit');
	const searchParam = new URLSearchParams(searchParams).get('search');
	const orderParam = new URLSearchParams(searchParams).get('order');
	const startDateParam = new URLSearchParams(searchParams).get('startDate');
	const endDateParam = new URLSearchParams(searchParams).get('endDate');
  	return defer({ allDocumentsData: getAllDocumentsData(pageParam, limitParam, searchParam, orderParam, startDateParam, endDateParam) });
};

const getSingleContract = async (id) => {
	const response = await DocumentService.getSingleContract(id);
	return response.data;
};

const singleContractLoader = async ({ params }) => {
  	const { id } = params;
	return defer({
	  	singleContract: getSingleContract(id),
	  	id,
	});
};

const getSingleAct = async (id) => {
	const response = await DocumentService.getSingleAct(id);
	return response.data;
};

const singleActLoader = async ({ params }) => {
	const { id } = params;
	return defer({
		singleAct: getSingleAct(id),
		id,
	});
};

const getAllPartners = async (page, limit, search) => {
	const response = await PartnerService.getAllPartners(page, limit, search);
	return response.data;
};

const allPartnersLoader = async ({ request }) => {
	const searchParams = request.url.split('?')[1];
	const pageParam = new URLSearchParams(searchParams).get('page');
	const limitParam = new URLSearchParams(searchParams).get('limit');
	const searchParam = new URLSearchParams(searchParams).get('search');
	return defer({ allPartners: getAllPartners(pageParam, limitParam, searchParam) });
};

const getSinglePartner = async (id) => {
	const response = await PartnerService.getSinglePartner(id);
	return response.data;
};

const singlePartnerLoader = async ({ params }) => {
	const { id } = params;
	return defer({
		singlePartner: getSinglePartner(id),
		id,
	});
};

export {
	allDocumentLoader, singleContractLoader, singleActLoader, allPartnersLoader, singlePartnerLoader,
};
