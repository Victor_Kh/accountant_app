import { useContext, useEffect } from 'react';
import Context from '../index';

const useContractsByPartnersId = (chosenPartner) => {
	const { globalStore: { getContractsByPartnersId } } = useContext(Context);

	useEffect(() => {
		getContractsByPartnersId(chosenPartner);
	}, [chosenPartner]);
};

export default useContractsByPartnersId;
