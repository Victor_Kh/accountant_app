import { useContext, useEffect } from 'react';
import { useSearchParams } from 'react-router-dom';
import useDebounce from './useDebounce';
import { resetFilter } from '../utils/helpers';
import Context from '../index';

const useFilters = (store) => {
	const [searchParams, setSearchParams] = useSearchParams();
	const debouncedSearchValue = useDebounce(store.searchValue);
	const { globalStore: { isSwitcherFirstTabVisible } } = useContext(Context);

	useEffect(() => {
		store.setPage(searchParams.get('page') || 1);
		store.setLimit(searchParams.get('limit') || 10);
		store.setSearchValue(searchParams.get('search') || '');
		store.setOrder && store.setOrder(searchParams.get('order') || '');
		store.setStartDate && store.setStartDate(searchParams.get('startDate') || null);
		store.setEndDate && store.setEndDate(searchParams.get('endDate') || null);
	}, []);

	useEffect(() => {
		const updatedSearchParams = {
			page: store.page,
			limit: store.limit,
			search: debouncedSearchValue,
			...(store.order ? { order: store.order } : {}),
			...(store.startDate && store.endDate && !store.dateError ? { startDate: store.startDate, endDate: store.endDate } : {}),
		};

		setSearchParams(updatedSearchParams);
	}, [store.page, store.limit, debouncedSearchValue, store.order, store.startDate, store.endDate, store.dateError]);

	useEffect(() => {
		return () => {
			resetFilter(store);
		};
	}, [isSwitcherFirstTabVisible]);
};

export default useFilters;
