import { useContext, useEffect, useState } from 'react';
import Context from '../index';

const useNotification = () => {
	const { userStore } = useContext(Context);
	const [isNotificationShown, setIsNotificationShown] = useState(false);
	useEffect(() => {
		if (userStore.notificationState.name || userStore.notificationState.errorMessage) {
			setIsNotificationShown(true);
		}
	}, [userStore.notificationState.name, userStore.notificationState.errorMessage]);

	return {
		isNotificationShown,
		setIsNotificationShown,
		notificationState: userStore.notificationState,
	};
};

export default useNotification;
