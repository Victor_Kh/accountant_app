import React, { Suspense, useContext } from 'react';
import {
	Await, Outlet, useLoaderData, useNavigate,
} from 'react-router-dom';
import { observer } from 'mobx-react-lite';
import Loader from '../components/common/loader/Loader';
import PartnersTable from '../components/PartnersTable/PartnersTable';
import Button from '../components/common/button/Button';
import Notification from '../components/common/notification/Notification';
import useNotification from '../hooks/useNotification';
import Pagination from '../components/common/pagination/Pagination';
import Context from '../index';
import Filter from '../components/common/filter/Filter';
import useFilters from '../hooks/useFilters';

const Partners = () => {
	const { allPartners } = useLoaderData();
	const { partnersFilterStore } = useContext(Context);
	const navigate = useNavigate();

	useFilters(partnersFilterStore);
	const addBtnHandler = () => {
		navigate('new_partner');
	};

	const { isNotificationShown, setIsNotificationShown, notificationState } = useNotification();

	const notification = isNotificationShown
		? <Notification
			setIsNotificationShown={setIsNotificationShown}
			message={`Контрагент ${notificationState.name} ${notificationState.action} успішно!`}/>
		: null;

	return (
		<div className='page'>
			{notification}
			<Filter store={partnersFilterStore} />
			<Suspense fallback={<Loader />}>
				<Await resolve={allPartners}>
					<PartnersTable />
					<Pagination store={partnersFilterStore} />
					<Button children={'Додати'} onclick={addBtnHandler} />
					<Outlet />
				</Await>
			</Suspense>
		</div>
	);
};

export default observer(Partners);
