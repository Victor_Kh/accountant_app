import React, { useContext } from 'react';
import { useAsyncValue, Form } from 'react-router-dom';
import Modal from '../components/common/modal/Modal';
import Button from '../components/common/button/Button';
import Context from '../index';

const DeletePartnerPage = () => {
	const { id, name } = useAsyncValue();
	const { userStore: { setNotificationState } } = useContext(Context);
	const clickHandler = () => {
		setNotificationState({ name, action: 'видалений' });
	};
	return (
		<Modal active={true}>
			<Form
				action={`/partner/${id}/delete`} method='delete'>
				<p>Ви впевнені, що бажаєте видалити?</p>
				<input type="hidden" name='id' defaultValue={id} />
				<Button type='submit' onClick={clickHandler}>Видалити</Button>
			</Form>
		</Modal>
	);
};

export default DeletePartnerPage;
