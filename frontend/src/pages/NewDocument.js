import React, { useContext, useEffect, useState } from 'react';
import { useAsyncValue, useOutletContext, useSubmit } from 'react-router-dom';
import { Formik } from 'formik';
import Select from 'react-select';
import { observer } from 'mobx-react-lite';
import formsValidationSchema from '../formsValidationSchema/formsValidationSchema';
import { currencySelectOptions } from '../utils/constants';
import Modal from '../components/common/modal/Modal';
import Input from '../components/common/input/Input';
import Button from '../components/common/button/Button';
import SelectField from '../components/common/select/SelectField';
import DatePickerField from '../components/common/datepicker/DatePickerField';
import Context from '../index';
import useContractsByPartnersId from '../hooks/useContractsByPartnersId';

const NewDocument = () => {
  	const { isSwitcherFirstTabVisible } = useOutletContext();
	const { data: { partners } } = useAsyncValue();
	const { globalStore: { contractsByPartnersId } } = useContext(Context);
	const contractSelectOptions = contractsByPartnersId.map((contract) => ({ value: contract.id, label: contract.name }));
	const partnersSelectOptions = partners.map((partner) => ({ value: partner.id, label: partner.name }));

	const [chosenPartner, setChosenPartner] = useState(null);

	useContractsByPartnersId(chosenPartner);

	const newContractFormInitialValues = {
		name: '',
		partnerId: '',
		dateOfSign: '',
		dateOfEnd: '',
		amount: '',
		currency: '',
	};
	const newActFormInitialValues = {
		name: '',
		partnerId: '',
		dateOfSign: '',
		amount: '',
		currency: '',
		ContractId: '',
	};
	const newContractFormValidationSchema = formsValidationSchema('newContractForm');
	const newActFormValidationSchema = formsValidationSchema('newActForm');
	const submit = useSubmit();
	const { userStore: { setNotificationState } } = useContext(Context);
 	const handleSubmit = async (values) => {
		submit(values, { method: 'post' });
		setNotificationState({
			name: values.name,
			action: 'створений',
			typeOfDocument: isSwitcherFirstTabVisible ? 'Контракт' : 'Акт',
		});
	};
	return (
		<Modal active={true}>
		   {isSwitcherFirstTabVisible
			   ? <Formik
				   initialValues={newContractFormInitialValues}
				   validationSchema={newContractFormValidationSchema}
				   onSubmit={handleSubmit}
			   >
				   {({ handleSubmit }) => (
					   <form action={'/documents/new_contract'} method='post' onSubmit={handleSubmit}>
						   <div className="form-row">
							   <Input label='Номер' name='name' id='name'/>
						   </div>
						   <div className="form-row">
							   <SelectField options={partnersSelectOptions} name='partnerId' label='Контрагент' id='partnerId'/>
						   </div>
						   <div className="form-row">
							   <DatePickerField name='dateOfSign' label='Дата підписання' id='dateOfSign' maxDate={new Date()}/>
						   </div>
						   <div className="form-row">
							   <DatePickerField name='dateOfEnd' label='Дата закінчення' id='dateOfEnd'/>
						   </div>
						   <div className="form-row">
							   <Input label='Сумма контракту' name='amount' id='amount'/>
						   </div>
						   <div className="form-row">
							   <SelectField options={currencySelectOptions} name='currency' label='Валюта контракту' id='currency'/>
						   </div>
						   <Button type='submit'>Додати контракт</Button>
					   </form>
				   )}
			   </Formik>
			   : <Formik
				   initialValues={newActFormInitialValues}
				   validationSchema={newActFormValidationSchema}
				   onSubmit={handleSubmit}
			   >
				   {({ handleSubmit, setFieldValue }) => (
					   <form action='/documents/new_act' method='post' onSubmit={handleSubmit}>
						   <div className="form-row">
							   <Input name='name' label='Номер' id='name'/>
						   </div>
						   <div className="form-row">
							   <div className="inputWrapper">
								   <label>
									   Контрагент
									   <div className="select-field">
										   <Select
											   options={partnersSelectOptions}
											   name='partnerId'
											   onChange={(newValue) => {
												   setChosenPartner(newValue.value);
												   setFieldValue('partnerId', newValue.value);
											   }}
										   />
									   </div>
								   </label>
							   </div>
						   </div>
						   <div className="form-row">
							   <DatePickerField name='dateOfSign' label='Дата підписання' id='dateOfEnd' maxDate={new Date()}/>
						   </div>
						   <div className="form-row">
							   <Input name='amount' label='Сумма акту'/>
						   </div>
						   <div className="form-row">
							   <SelectField options={currencySelectOptions} name='currency' label='Валюта акту' id='currency'/>
						   </div>
						   <div className="form-row">
							   <SelectField options={contractSelectOptions} name='ContractId' label='Контракт' id='ContractId'/>
						   </div>
						   <div className="form-row">
							   <Button type='submit'>Додати акт</Button>
						   </div>
					   </form>
				   )}
			   </Formik>}
		</Modal>
	);
};

export default observer(NewDocument);
