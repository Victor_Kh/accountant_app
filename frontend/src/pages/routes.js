import { createBrowserRouter } from 'react-router-dom';
import Auth from './Auth';
import Documents from './Documents';
import Home from './Home';
import ContractPage from './ContractPage';
import NewDocument from './NewDocument';
import Layout from '../components/Layout/Layout';
import PrivateRoute from '../hoc/PrivateRoute';
import ActPage from './ActPage';
import ErrorPage from './ErrorPage';
import {
	allDocumentLoader, singleContractLoader, singleActLoader, allPartnersLoader, singlePartnerLoader,
} from '../loaders/loaders';
import {
	newContractAction,
	newActAction,
	editContractAction,
	editActAction,
	deleteActAction,
	deleteContractAction,
	newPartnerAction,
	deletePartnerAction,
	editPartnerAction,
} from '../actions/actions';
import EditContractPage from './EditContractPage';
import EditActPage from './EditActPage';
import DeleteDocumentPage from './DeleteDocumentPage';
import Partners from './Partners';
import PartnerPage from './PartnerPage';
import NewPartner from './NewPartner';
import DeletePartnerPage from './DeletePartnerPage';
import EditPartnerPage from './EditPartnerPage';

const router = createBrowserRouter([
  	{
		path: '/',
		element: <Layout />,
		errorElement: <ErrorPage />,
		children: [
			{
				path: '/',
				element: <Home />,
				index: true,
			},
			{
				path: 'login',
				element: <Auth />,
			},
			{
				path: 'documents',
				element:
				  	<PrivateRoute>
					  <Documents />
				  	</PrivateRoute>,
			  	children: [
					{
						path: 'new_contract',
						element:
							<PrivateRoute>
							  <NewDocument />
							</PrivateRoute>,
						action: newContractAction,
					},
					{
						path: 'new_act',
						element:
							<PrivateRoute>
							  <NewDocument />
							</PrivateRoute>,
						action: newActAction,
					},
				],
			  	loader: allDocumentLoader,
			},
			{
				path: 'contract/:id',
				element:
				  <PrivateRoute>
				  	<ContractPage />
				  </PrivateRoute>,
				loader: singleContractLoader,
				children: [
					{
						path: 'edit',
						element:
							<PrivateRoute>
								<EditContractPage />
							</PrivateRoute>,
						action: editContractAction,
					},
					{
						path: 'delete',
						element:
							<PrivateRoute>
								<DeleteDocumentPage />
							</PrivateRoute>,
						action: deleteContractAction,
					},
				],
			},
			{
				path: 'act/:id',
				element:
					<PrivateRoute>
						<ActPage />
					</PrivateRoute>,
				loader: singleActLoader,
				children: [
					{
						path: 'edit',
						element:
							<PrivateRoute>
								<EditActPage />
							</PrivateRoute>,
						action: editActAction,
					},
					{
						path: 'delete',
						element:
							<PrivateRoute>
								<DeleteDocumentPage />
							</PrivateRoute>,
						 action: deleteActAction,
					},
				],
			},
			{
				path: 'partners',
				element:
					<PrivateRoute>
						<Partners />
					</PrivateRoute>,
				children: [
					{
						path: 'new_partner',
						element:
							<PrivateRoute>
								<NewPartner />
							</PrivateRoute>,
						action: newPartnerAction,
					},
				],
				loader: allPartnersLoader,
			},
			{
				path: 'partner/:id',
				element:
					<PrivateRoute>
						<PartnerPage />
					</PrivateRoute>,
				loader: singlePartnerLoader,
				children: [
					{
						path: 'edit',
						element:
							<PrivateRoute>
								<EditPartnerPage />
							</PrivateRoute>,
						action: editPartnerAction,
					},
					{
						path: 'delete',
						element:
							<PrivateRoute>
								<DeletePartnerPage />
							</PrivateRoute>,
						action: deletePartnerAction,
					},
				],
			},
		],
	},
]);

export default router;
