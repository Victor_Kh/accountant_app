import React, { useContext } from 'react';
import { useOutletContext, Form, useAsyncValue } from 'react-router-dom';
import Modal from '../components/common/modal/Modal';
import Button from '../components/common/button/Button';
import Context from '../index';

const DeleteDocumentPage = () => {
	const {
		documentType, id,
	} = useOutletContext();
	const { contract, completedWorkAct } = useAsyncValue();
	const action = documentType === 'contract' ? `/contract/${id}/delete` : `/act/${id}/delete`;
	const name = documentType === 'contract' ? contract.name : completedWorkAct.name;
	const typeOfDocument = documentType === 'contract' ? 'контракт' : 'акт';
	const { userStore: { setNotificationState } } = useContext(Context);
	const clickHandler = () => {
		setNotificationState({
			name,
			action: 'видалений',
			typeOfDocument: typeOfDocument.charAt(0).toUpperCase() + typeOfDocument.slice(1),
		});
	};
	return (
		<Modal active={true} >
			<Form action={action} method='delete'>
				<input type="hidden" name='id' defaultValue={id}/>
				<p>Ви впевнені, що бажаєте видалити?</p>
				<div className="form-row">
					<Button type='submit' onClick={clickHandler}>Видалити {typeOfDocument}</Button>
				</div>
			</Form>
		</Modal>
	);
};

export default DeleteDocumentPage;
