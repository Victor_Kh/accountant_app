import React, { Suspense } from 'react';
import {
	useLoaderData, Await, useNavigate, Outlet,
} from 'react-router-dom';
import Document from '../components/Document/Document';
import Button from '../components/common/button/Button';
import Loader from '../components/common/loader/Loader';
import useNotification from '../hooks/useNotification';
import Notification from '../components/common/notification/Notification';

const ContractPage = () => {
  	const { singleContract, id } = useLoaderData();
  	const navigate = useNavigate();

	const editBtnHandler = () => {
	  	navigate(`/contract/${id}/edit`);
	};
	const deleteBtnHandler = () => {
		navigate(`/contract/${id}/delete`);
	};
	const { isNotificationShown, setIsNotificationShown, notificationState } = useNotification();
	const notification = isNotificationShown
		? <Notification
			setIsNotificationShown={setIsNotificationShown}
			message={`Контракт ${notificationState.name} ${notificationState.action} успішно!`}/>
		: null;
	return (
		<div className='entity-page'>
			{notification}
			<Suspense fallback={<Loader />}>
				<Await resolve={singleContract}>
					<Document type={'Контракт'} />
					<Button children={'Змінити'} onClick={editBtnHandler} />
					<Button children={'Видалити'} onClick={deleteBtnHandler}/>
					<Button children={'Переглянути'} />
					<Outlet context={{ documentType: 'contract', id }}/>
				</Await>
			</Suspense>
		</div>
	);
};

export default ContractPage;
