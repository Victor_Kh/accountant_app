import React, { useContext } from 'react';
import { observer } from 'mobx-react-lite';
import { Navigate } from 'react-router-dom';
import RegistrationForm from '../components/RegistrationForm/RegistrationForm';
import LoginForm from '../components/LoginForm/LoginForm';
import Context from '../index';
import Switcher from '../components/Switcher/Switcher';
import useNotification from '../hooks/useNotification';
import Notification from '../components/common/notification/Notification';

const Auth = () => {
	const { userStore, globalStore } = useContext(Context);

	const { isNotificationShown, setIsNotificationShown, notificationState } = useNotification();

	const notification = isNotificationShown
		? <Notification
			setIsNotificationShown={setIsNotificationShown}
			message={notificationState.errorMessage}
			isError={notificationState.isError}
		/> : null;

	if (userStore.isAuth) {
		return (
	  		<Navigate to={'/'} />
		);
	}
	return (
		<>
			{notification}
			<Switcher firstTitle={'Увійти'} secondTitle={'Зареєструватися'}/>
			{globalStore.isSwitcherFirstTabVisible
				  ? <LoginForm />
				  : <RegistrationForm />}
		</>
	);
};

export default observer(Auth);
