import React, { useContext } from 'react';
import { Formik } from 'formik';
import { useSubmit } from 'react-router-dom';
import Modal from '../components/common/modal/Modal';
import Input from '../components/common/input/Input';
import Button from '../components/common/button/Button';
import FormPhoneInput from '../components/common/phoneInput/FormPhoneInput';
import formsValidationSchema from '../formsValidationSchema/formsValidationSchema';
import Context from '../index';
import BankInfoForm from '../components/BankInfo/BankInfoForm';
import ManagersInfoForm from '../components/ManagersInfoForm/ManagersInfoForm';

const NewPartner = () => {
	const submit = useSubmit();
	const newPartnerForminitialValues = {
		name: '',
		partnerId: '',
		partnerTaxNumber: '',
		email: '',
		phone: '',
		banksInfo: [{ IBAN: '', name: '' }],
		managersInfo: [{ position: '', name: '', actingBy: '' }],
		registrationAddress: '',
		actualAddress: '',
	};
	const { userStore: { setNotificationState } } = useContext(Context);
	const handleSubmit = (values) => {
		values = {
			...values,
			banksInfo: JSON.stringify(values.banksInfo),
			managersInfo: JSON.stringify(values.managersInfo),
		};
		submit(values, { method: 'post' });
		setNotificationState({ name: values.name, action: 'створений' });
	};
	const newPartnerFormValidationSchema = formsValidationSchema('newPartnerForm');
	return (
		<Modal active={true}>
			<Formik
				onSubmit={handleSubmit}
				validationSchema={newPartnerFormValidationSchema}
				initialValues={newPartnerForminitialValues}
			>
				{({
					handleSubmit, values,
				}) => (
					<form action={'/partners/new_partner'} method='post' onSubmit={handleSubmit}>
						<div className='form-row'>
							<Input
								label='Назва'
								name='name' id='name'/>
						</div>
						<div className='form-row'>
							<Input label='Код ЄДРПОУ' name='partnerId' id='partnerId'/>
						</div>
						<div className='form-row'>
							<Input label='ІПН' name='partnerTaxNumber' id='partnerTaxNumber'/>
						</div>
						<div className='form-row'>
							<Input label='Електронна пошта' name='email' id='email'/>
						</div>
						<div className='form-row'>
							<Input label='Юридична адреса' name='registrationAddress' id='registrationAddress'/>
						</div>
						<div className='form-row'>
							<Input label='Фактична адреса' name='actualAddress' id='actualAddress'/>
						</div>
						<div className='form-row'>
							<FormPhoneInput label='Контактний телефон' name='phone' id='phone'/>
						</div>
						<BankInfoForm values={values} />
						<ManagersInfoForm values={values} />
						<Button type='submit'>Додати партнера</Button>
					</form>
				)}
			</Formik>
		</Modal>
	);
};

export default NewPartner;
