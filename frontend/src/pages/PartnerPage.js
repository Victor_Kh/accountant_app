import React, { Suspense } from 'react';
import {
	Await, Outlet, useLoaderData, useNavigate,
} from 'react-router-dom';
import Loader from '../components/common/loader/Loader';
import Partner from '../components/Partner/Partner';
import Button from '../components/common/button/Button';
import Notification from '../components/common/notification/Notification';
import useNotification from '../hooks/useNotification';

const PartnerPage = () => {
	const { singlePartner, id } = useLoaderData();
	const navigate = useNavigate();
	const { isNotificationShown, setIsNotificationShown, notificationState } = useNotification();
	const notification = isNotificationShown
		? <Notification
			setIsNotificationShown={setIsNotificationShown}
			message={`Контрагент ${notificationState.name} ${notificationState.action} успішно!`}/>
		: null;
	return (
		<Suspense fallback={<Loader />}>
			<Await resolve={singlePartner}>
				<div className='entity-page'>
					{notification}
					<Partner />
					<Button onclick={() => navigate(`/partner/${id}/edit`)}>Змінити</Button>
					<Button onclick={() => navigate(`/partner/${id}/delete`)}>Видалити</Button>
					<Outlet />
				</div>
			</Await>
		</Suspense>
	);
};

export default PartnerPage;
