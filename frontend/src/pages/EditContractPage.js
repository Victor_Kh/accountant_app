import React, { useContext } from 'react';
import { useAsyncValue, useSubmit } from 'react-router-dom';
import { Formik } from 'formik';
import { parseISO } from 'date-fns';
import { currencySelectOptions, documentStatusSelectOptions } from '../utils/constants';
import Modal from '../components/common/modal/Modal';
import Input from '../components/common/input/Input';
import Button from '../components/common/button/Button';
import Context from '../index';
import formsValidationSchema from '../formsValidationSchema/formsValidationSchema';
import SelectField from '../components/common/select/SelectField';
import DatePickerField from '../components/common/datepicker/DatePickerField';

const EditContractPage = () => {
	const singleContract = useAsyncValue();
	const {
		contract: {
			id, name, dateOfSign, dateOfEnd, isActive, contractAmount, currencyOfContract, PartnerId,
		},
	} = singleContract;

	const { globalStore: { allPartners }, userStore: { setNotificationState } } = useContext(Context);
	const partnerSelectOptions = allPartners.map((partner) => ({ value: partner.id, label: partner.name }));
	const submit = useSubmit();
	const editContractFormInitialValue = {
		id,
		name,
		partnerId: PartnerId,
		dateOfSign: parseISO(dateOfSign),
		dateOfEnd: parseISO(dateOfEnd),
		amount: contractAmount,
		currency: currencyOfContract,
		isActive,
	};

	const editContractFormValidationSchema = formsValidationSchema('editContractForm');

	const handleSubmit = async (values) => {
		submit(values, { method: 'put' });
		setNotificationState({ name: values.name, action: 'змінений' });
	};

	return (
		<Modal active={true}>
			<Formik
				initialValues={editContractFormInitialValue}
				validationSchema={editContractFormValidationSchema}
				onSubmit={handleSubmit}
			>
				{({ handleSubmit, values }) => (
					<form action={`/contract/${id}/edit`} method='put' onSubmit={handleSubmit}>
						<Input type="hidden" name='id' value={values.id}/>
						<div className="form-row">
							<Input label='Номер' name='name' id='name' value={values.name}/>
						</div>
						<div className="form-row">
							<SelectField
								label={'Контрагент'}
								options={partnerSelectOptions}
								name='partnerId'
							/>
						</div>
						<div className="form-row">
							<DatePickerField
								name='dateOfSign'
								label='Дата підписання'
								id='dateOfSign'
								maxDate={new Date()}
							/>
						</div>
						<div className="form-row">
							<DatePickerField
								name='dateOfEnd'
								label='Дата закінчення'
								id='dateOfEnd'
							/>
						</div>
						<div className="form-row">
							<Input label='Сумма контракту' name='amount' id='amount' value={values.amount}/>
						</div>
						<div className="form-row">
							<SelectField
								label={'Валюта контракту'}
								options={currencySelectOptions}
								name='currency'
							/>
						</div>
						<div className="form-row">
							<SelectField
								label={'Статус виконання'}
								options={documentStatusSelectOptions}
								name='isActive'
							/>
						</div>
						<Button type='submit'>Змінити контракт</Button>
					</form>
				)}
			</Formik>
		</Modal>
	);
};

export default EditContractPage;
