import React, { useContext } from 'react';
import { Formik } from 'formik';
import { useAsyncValue, useSubmit } from 'react-router-dom';
import Button from '../components/common/button/Button';
import Modal from '../components/common/modal/Modal';
import Input from '../components/common/input/Input';
import formsValidationSchema from '../formsValidationSchema/formsValidationSchema';
import FormPhoneInput from '../components/common/phoneInput/FormPhoneInput';
import Context from '../index';
import BankInfoForm from '../components/BankInfo/BankInfoForm';
import ManagersInfoForm from '../components/ManagersInfoForm/ManagersInfoForm';

const EditPartnerPage = () => {
	const {
		id, name, partnerId, partnerTaxNumber, email, phone, banksInfo, managersInfo, registrationAddress, actualAddress,
	} = useAsyncValue();
	const editPartnerFormInitialValue = {
		id,
		name,
		partnerId,
		partnerTaxNumber,
		email,
		phone,
		banksInfo,
		managersInfo,
		registrationAddress,
		actualAddress,
		deletedManagers: [],
		updatedManagers: [],
		deletedBanks: [],
		updatedBanks: [],
	};
	const editPartnerFormValidationSchema = formsValidationSchema('newPartnerForm');
	const submit = useSubmit();
	const { userStore: { setNotificationState } } = useContext(Context);
	const handleSubmit = (values) => {
		const updatedManagers = values.managersInfo
			.filter((managerInfo, index) => {
				return managerInfo.id && JSON.stringify(managerInfo) !== JSON.stringify(editPartnerFormInitialValue.managersInfo[index]);
			});
		const updatedBanks = values.banksInfo
			.filter((bankInfo, index) => {
				return bankInfo.id && JSON.stringify(bankInfo) !== JSON.stringify(editPartnerFormInitialValue.banksInfo[index]);
			});
		values = {
			...values,
			banksInfo: JSON.stringify(values.banksInfo),
			managersInfo: JSON.stringify(values.managersInfo),
			deletedManagers: JSON.stringify(values.deletedManagers),
			updatedManagers: JSON.stringify(updatedManagers),
			deletedBanks: JSON.stringify(values.deletedBanks),
			updatedBanks: JSON.stringify(updatedBanks),
		};
		submit(values, { method: 'put' });
		setNotificationState({ name: values.name, action: 'змінений' });
	};
	return (
		<Modal active={true}>
			<Formik
				initialValues={editPartnerFormInitialValue}
				validationSchema={editPartnerFormValidationSchema}
				onSubmit={handleSubmit}
			>
				{({ handleSubmit, values }) => (
					<form action={`/partner/${id}/edit`} method='put' onSubmit={handleSubmit}>
						<input type="hidden" name='id' value={values.id}/>
						<div className="form-row">
							<Input label='Контрагент' name='name' value={values.name} id='name' />
						</div>
						<div className="form-row">
							<Input type='text' label='Код ЄДРПОУ' name='partnerId' value={values.partnerId} id='partnerId' />
						</div>
						<div className="form-row">
							<Input type='text' label='ІПН' name='partnerTaxNumber' value={values.partnerTaxNumber} id='partnerTaxNumber' />
						</div>
						<div className="form-row">
							<Input type='text' label='Електронна пошта' name='email' value={values.email} id='email' />
						</div>
						<div className='form-row'>
							<Input label='Юридична адреса' name='registrationAddress' value={values.registrationAddress} id='registrationAddress'/>
						</div>
						<div className='form-row'>
							<Input label='Фактична адреса' name='actualAddress' value={values.actualAddress} id='actualAddress'/>
						</div>
						<div className="form-row">
							<FormPhoneInput label='Контактний телефон' name='phone' id='phone' />
						</div>
						<BankInfoForm values={values} />
						<ManagersInfoForm values={values} />
						<Button type='submit'>Змінити</Button>
					</form>
				)}
			</Formik>
		</Modal>
	);
};

export default EditPartnerPage;
