import React, { useContext, useEffect, useState } from 'react';
import { useAsyncValue, useSubmit } from 'react-router-dom';
import { Formik } from 'formik';
import { parseISO } from 'date-fns';
import Select from 'react-select';
import { observer } from 'mobx-react-lite';
import { currencySelectOptions, documentStatusSelectOptions } from '../utils/constants';
import Modal from '../components/common/modal/Modal';
import Input from '../components/common/input/Input';
import Button from '../components/common/button/Button';
import Context from '../index';
import SelectField from '../components/common/select/SelectField';
import DatePickerField from '../components/common/datepicker/DatePickerField';
import formsValidationSchema from '../formsValidationSchema/formsValidationSchema';
import useContractsByPartnersId from '../hooks/useContractsByPartnersId';

const EditActPage = () => {
	const singleAct = useAsyncValue();
	const {
		completedWorkAct: {
			id, name, dateOfSign, isActive, completedWorkActAmount, currencyOfCompletedWorkAct, ContractId, PartnerId,
		},
	} = singleAct;

	const [chosenPartner, setChosenPartner] = useState(PartnerId);

	useContractsByPartnersId(chosenPartner);

	const {
		globalStore: { allPartners, contractsByPartnersId }, userStore: { setNotificationState },
	} = useContext(Context);

	const chosenSelect = { value: chosenPartner, label: allPartners.filter((partner) => partner.id === chosenPartner)[0].name };
	const contractSelectOptions = contractsByPartnersId.map((contract) => ({ value: contract.id, label: contract.name }));
	const partnerSelectOptions = allPartners.map((partner) => ({ value: partner.id, label: partner.name }));
	const submit = useSubmit();

	const editActFormInitialValues = {
		id,
		name,
		PartnerId,
		dateOfSign: parseISO(dateOfSign),
		amount: completedWorkActAmount,
		currency: currencyOfCompletedWorkAct,
		ContractId,
		isActive,
	};

	const editActFormValidationSchema = formsValidationSchema('editActForm');

	const handleSubmit = (values) => {
		submit(values, { method: 'put' });
		setNotificationState({ name: values.name, action: 'змінений' });
	};
	return (
		<Modal active={true}>
			<Formik
				initialValues={editActFormInitialValues}
				validationSchema={editActFormValidationSchema}
				onSubmit={handleSubmit}
			>
				{({ handleSubmit, values, setFieldValue }) => (
					<form action={`/act/${id}/edit`} method='post' onSubmit={handleSubmit}>
						<input type="hidden" name='id' value={values.id}/>
						<div className="form-row">
							<Input label='Номер' name='name' value={values.name} id='name'/>
						</div>
						<div className="form-row">
							<div className="inputWrapper">
								<label>
									Контрагент
									<div className="select-field">
										<Select
											options={partnerSelectOptions}
											name='PartnerId'
											value={chosenSelect}
											onChange={(newValue) => {
												setChosenPartner(newValue.value);
												setFieldValue('PartnerId', newValue.value);
											}}
										/>
									</div>
								</label>
							</div>
						</div>
						<div className="form-row">
							<DatePickerField
								name='dateOfSign'
								label='Дата підписання'
								id='dateOfSign'
								maxDate={new Date()}
							/>
						</div>
						<div className="form-row">
							<Input label='Сумма акту' name='amount' id='amount' value={values.amount}/>
						</div>
						<div className="form-row">
							<SelectField
								options={currencySelectOptions}
								name='currency'
								label='Валюта акту'
								id='currency'
							/>
						</div>
						<div className="form-row">
							<SelectField
								options={contractSelectOptions}
								name='ContractId'
								label='Контракт'
								id='ContractId'
							/>
						</div>
						<div className="form-row">
							<SelectField
								options={documentStatusSelectOptions}
								name='isActive'
								label='Статус виконання'
								id='isActive'
							/>
						</div>
						<Button type='submit'>Змінити акт</Button>
					</form>
				)}
			</Formik>
		</Modal>
	);
};

export default observer(EditActPage);
