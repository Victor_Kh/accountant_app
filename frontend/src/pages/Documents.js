import React, { Suspense, useContext } from 'react';
import {
	useLoaderData, Await, useNavigate, Outlet,
} from 'react-router-dom';
import { observer } from 'mobx-react-lite';
import Context from '../index';
import DocumentsTable from '../components/DocumentsTable/DocumentsTable';
import Switcher from '../components/Switcher/Switcher';
import Button from '../components/common/button/Button';
import Loader from '../components/common/loader/Loader';
import useNotification from '../hooks/useNotification';
import Notification from '../components/common/notification/Notification';
import Pagination from '../components/common/pagination/Pagination';
import DocumentsFilter from '../components/common/filter/DocumentsFilter';
import Filter from '../components/common/filter/Filter';
import useFilters from '../hooks/useFilters';

const Documents = () => {
	const { allDocumentsData } = useLoaderData();
	const navigate = useNavigate();
	const { globalStore, documentsFilterStore } = useContext(Context);
	const { isSwitcherFirstTabVisible } = globalStore;

	useFilters(documentsFilterStore);
	const clickBtnHandler = () => {
		navigate(isSwitcherFirstTabVisible ? 'new_contract' : 'new_act');
	};

	const { isNotificationShown, setIsNotificationShown, notificationState } = useNotification();

	const notification = isNotificationShown
		? <Notification
			setIsNotificationShown={setIsNotificationShown}
			message={`${notificationState.typeOfDocument} ${notificationState.name} ${notificationState.action} успішно!`}/>
		: null;

	return (
		<div className='page'>
			{notification}
			<Filter store={documentsFilterStore} render={<DocumentsFilter />} />
			<Suspense fallback={<Loader />}>
				<Await resolve={allDocumentsData}>
					<Switcher firstTitle={'Контракти'} secondTitle={'Акти виконаних робіт'} />
					<DocumentsTable isContractsVisible={isSwitcherFirstTabVisible} />
					<Pagination store={documentsFilterStore} />
					<Button children={'Додати'} onclick={clickBtnHandler} />
					<Outlet context={{ isSwitcherFirstTabVisible }} />
				</Await>
			</Suspense>
		</div>
	);
};

export default observer(Documents);
