import React, { Suspense } from 'react';
import {
	useLoaderData, Await, Outlet, useNavigate,
} from 'react-router-dom';
import Document from '../components/Document/Document';
import Button from '../components/common/button/Button';
import Loader from '../components/common/loader/Loader';
import useNotification from '../hooks/useNotification';
import Notification from '../components/common/notification/Notification';

const ActPage = () => {
	const { singleAct, id } = useLoaderData();
	const navigate = useNavigate();

	const editBtnHandler = () => {
		navigate(`/act/${id}/edit`);
	};

	const deleteBtnHandler = () => {
		navigate(`/act/${id}/delete`);
	};
	const { isNotificationShown, setIsNotificationShown, notificationState } = useNotification();
	const notification = isNotificationShown
		? <Notification
			setIsNotificationShown={setIsNotificationShown}
			message={`Акт ${notificationState.name} ${notificationState.action} успішно!`}/>
		: null;
	return (
		<div className='entity-page'>
			{notification}
			<Suspense fallback={<Loader />}>
				<Await resolve={singleAct}>
					<Document type={'Акт'} />
					<Button children={'Змінити'} onClick={editBtnHandler} />
					<Button children={'Видалити'} onClick={deleteBtnHandler} />
					<Button children={'Переглянути'} />
					<Outlet context={{
						id, documentType: 'act',
					}}/>
				</Await>
			</Suspense>
		</div>
	);
};

export default ActPage;
