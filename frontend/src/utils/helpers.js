const classNames = (classNames) => {
	return classNames.join(' ');
};

export const resetFilter = (store) => {
	store.setLimit(10);
	store.setSearchValue('');
	store.setOrder && store.setOrder('');
	store.setStartDate && store.setStartDate(null);
	store.setEndDate && store.setEndDate(null);
};

export default classNames;
