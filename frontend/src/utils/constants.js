const BASE_URL = 'http://localhost:3001';

export const currencySelectOptions = [
	{
		value: 'USD',
		label: 'USD',
	},
	{
		value: 'UAH',
		label: 'UAH',
	},
];

export const documentStatusSelectOptions = [
	{
		value: true,
		label: 'Виконаний',
	},
	{
		value: false,
		label: 'Не виконаний',
	},
];

export default BASE_URL;
