import React, { useContext } from 'react';
import { Navigate } from 'react-router-dom';
import { observer } from 'mobx-react-lite';
import Context from '../index';
import Loader from '../components/common/loader/Loader';

const PrivateRoute = ({ children }) => {
  	const { userStore } = useContext(Context);
	const { isAuth, isLoading } = userStore;
	if (isLoading) {
		return <Loader />;
	}

	if (isAuth === false) {
	  	return <Navigate to='/login' />;
	}
	return children;
};

export default observer(PrivateRoute);
