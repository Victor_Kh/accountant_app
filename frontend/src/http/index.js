import axios from 'axios';
import BASE_URL from '../utils/constants';

const api = axios.create({
	withCredentials: true,
	baseURL: BASE_URL,
});

api.interceptors.request.use((config) => {
	config.headers.Authorization = `Bearer ${localStorage.getItem('accessToken')}`;
	return config;
});

api.interceptors.response.use((config) => {
  	return config;
}, async (error) => {
  	const originalRequest = error.config;
  	if (error.response.status === 401 && error.config && !error.config.isRetry) {
		originalRequest.isRetry = true;
		try {
			const response = await axios.get(`${BASE_URL}/refresh`, { withCredentials: true });
			localStorage.setItem('accessToken', response.data.accessToken);
			return api.request(originalRequest);
		}
		catch (e) {
		  	throw new Error(e);
		}
	}
	return error;
});

export default api;
