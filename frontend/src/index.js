import React, { createContext } from 'react';
import ReactDOM from 'react-dom/client';
import './index.scss';
import App from './App';
import UserStore from './store/user';
import GlobalStore from './store/global';
import PartnersFilterStore from './store/partners_filter';
import DocumentsFilterStore from './store/document_filter';

const Context = createContext(null);

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
	<Context.Provider value={{
		userStore: new UserStore(),
		globalStore: new GlobalStore(),
		partnersFilterStore: new PartnersFilterStore(),
		documentsFilterStore: new DocumentsFilterStore(),
	}}>
		<App />
	</Context.Provider>,
);

export default Context;
