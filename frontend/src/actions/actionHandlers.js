import DocumentService from '../services/documentService';
import PartnerService from '../services/partnerService';

export const newContractHandler = async (contract) => {
	try {
		const response = await DocumentService.addNewContract(contract);
		return response.data;
	}
	catch (e) {
		throw new Error(e);
	}
};

export const newActHandler = async (act) => {
	try {
		const response = await DocumentService.addNewAct(act);
		return response.data;
	}
	catch (e) {
		throw new Error(e);
	}
};

export const editContractHandler = async (contract, id) => {
	try {
		const response = await DocumentService.editContract(contract, id);
		return response.data;
	}
	catch (e) {
		throw new Error(e);
	}
};

export const editActHandler = async (act, id) => {
	try {
		const response = await DocumentService.editAct(act, id);
		return response.data;
	}
	catch (e) {
		throw new Error(e);
	}
};

export const deleteActHandler = async (id) => {
	try {
		const response = await DocumentService.deleteAct(id);
		return response.data;
	}
	catch (e) {
		throw new Error(e);
	}
};

export const deleteContractHandler = async (id) => {
	try {
		const response = await DocumentService.deleteContract(id);
		return response.data;
	}
	catch (e) {
		throw new Error(e);
	}
};

export const newPartnerHandler = async (partner) => {
	try {
		const response = await PartnerService.addNewPartner(partner);
		return response.data;
	}
	catch (e) {
		throw new Error(e);
	}
};

export const deletePartnerHandler = async (id) => {
	try {
		const response = await PartnerService.deletePartner(id);
		return response.data;
	}
	catch (e) {
		throw new Error(e);
	}
};

export const editPartnerHandler = async (partner, id) => {
	try {
		const response = await PartnerService.editPartner(partner, id);
		return response.data;
	}
	catch (e) {
		throw new Error(e);
	}
};
