import { redirect } from 'react-router-dom';
import {
	deleteActHandler,
	deleteContractHandler, deletePartnerHandler,
	editActHandler,
	editContractHandler, editPartnerHandler,
	newActHandler,
	newContractHandler,
	newPartnerHandler,
} from './actionHandlers';

const newContractAction = async ({ request }) => {
	const formData = await request.formData();
	const newContract = {
		name: formData.get('name'),
		partner: formData.get('partner'),
		PartnerId: formData.get('partnerId'),
		dateOfSign: formData.get('dateOfSign'),
		dateOfEnd: formData.get('dateOfEnd'),
		contractAmount: formData.get('amount'),
		currencyOfContract: formData.get('currency'),
		isActive: false,
	};
	await newContractHandler(newContract);
	return redirect('/documents');
};
const newActAction = async ({ request }) => {
	const formData = await request.formData();
	const newAct = {
		name: formData.get('name'),
		partner: formData.get('partner'),
		PartnerId: formData.get('partnerId'),
		dateOfSign: formData.get('dateOfSign'),
		completedWorkActAmount: formData.get('amount'),
		currencyOfCompletedWorkAct: formData.get('currency'),
		isActive: true,
		ContractId: formData.get('ContractId'),
	};
	await newActHandler(newAct);
	return redirect('/documents');
};

const editContractAction = async ({ request }) => {
	const formData = await request.formData();
	const newContract = {
		id: formData.get('id'),
		name: formData.get('name'),
		PartnerId: formData.get('partnerId'),
		dateOfSign: formData.get('dateOfSign'),
		dateOfEnd: formData.get('dateOfEnd'),
		contractAmount: formData.get('amount'),
		currencyOfContract: formData.get('currency'),
		isActive: formData.get('isActive'),
	};
	await editContractHandler(newContract, newContract.id);
	return redirect(`/contract/${newContract.id}`);
};

const editActAction = async ({ request }) => {
	const formData = await request.formData();
	const newAct = {
		id: formData.get('id'),
		name: formData.get('name'),
		PartnerId: formData.get('PartnerId'),
		dateOfSign: formData.get('dateOfSign'),
		completedWorkActAmount: formData.get('amount'),
		currencyOfCompletedWorkAct: formData.get('currency'),
		isActive: formData.get('isActive'),
		ContractId: formData.get('ContractId'),
	};
	await editActHandler(newAct, newAct.id);
	return redirect(`/act/${newAct.id}`);
};

const deleteActAction = async ({ request }) => {
	const formData = await request.formData();
	const id = formData.get('id');
	await deleteActHandler(id);
	return redirect('/documents');
};

const deleteContractAction = async ({ request }) => {
	const formData = await request.formData();
	const id = formData.get('id');
	await deleteContractHandler(id);
	return redirect('/documents');
};

const newPartnerAction = async ({ request }) => {
	const formData = await request.formData();
	const newPartner = {
		name: formData.get('name'),
		partnerId: formData.get('partnerId'),
		partnerTaxNumber: formData.get('partnerTaxNumber'),
		email: formData.get('email'),
		phone: formData.get('phone'),
		banksInfo: formData.get('banksInfo'),
		managersInfo: formData.get('managersInfo'),
		registrationAddress: formData.get('registrationAddress'),
		actualAddress: formData.get('actualAddress'),
	};
	await newPartnerHandler(newPartner);
	return redirect('/partners');
};

const deletePartnerAction = async ({ request }) => {
	const formData = await request.formData();
	const id = formData.get('id');
	await deletePartnerHandler(id);
	return redirect('/partners');
};

const editPartnerAction = async ({ request }) => {
	const formData = await request.formData();
	const newPartner = {
		id: formData.get('id'),
		name: formData.get('name'),
		partnerId: formData.get('partnerId'),
		partnerTaxNumber: formData.get('partnerTaxNumber'),
		email: formData.get('email'),
		phone: formData.get('phone'),
		banksInfo: formData.get('banksInfo'),
		managersInfo: formData.get('managersInfo'),
		deletedManagers: formData.get('deletedManagers'),
		updatedManagers: formData.get('updatedManagers'),
		deletedBanks: formData.get('deletedBanks'),
		updatedBanks: formData.get('updatedBanks'),
		registrationAddress: formData.get('registrationAddress'),
		actualAddress: formData.get('actualAddress'),
	};
	await editPartnerHandler(newPartner, newPartner.id);
	return redirect(`/partner/${newPartner.id}`);
};

export {
	newContractAction,
	newActAction,
	editContractAction,
	editActAction,
	deleteActAction,
	deleteContractAction,
	newPartnerAction,
	deletePartnerAction,
	editPartnerAction,
};
