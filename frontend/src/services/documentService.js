import api from '../http';

class DocumentService {
	async getAllDocuments(page, limit, search, order, startDate, endDate) {
		return api.get('/all_documents', {
			params: {
				page, limit, search, order, startDate, endDate,
			},
		});
	}

	async getSingleContract(id) {
	  return api.get(`get_contract/${id}`);
	}

	async getSingleAct(id) {
		return api.get(`/get_completedWorkAct/${id}`);
	}

	async addNewContract(contract) {
	  return api.post('/add_contract', contract);
	}

	async addNewAct(act) {
		return api.post('add_completedWorkAct', act);
	}

	async editContract(contract, id) {
		return api.put(`/update_contract/${id}`, contract);
	}

	async editAct(act, id) {
		return api.put(`/update_completedWorkAct/${id}`, act);
	}

	async deleteAct(id) {
		return api.delete(`/remove_completedWorkAct/${id}`);
	}

	async deleteContract(id) {
		return api.delete(`/remove_contract/${id}`);
	}

	async getContractsByPartnersId(partnerId) {
		return api.get(`/contractsByPartnersId/${partnerId}`);
	}
}

export default new DocumentService();
