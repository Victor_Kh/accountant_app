import api from '../http';

class AuthService {
	async registration(name, email, password) {
		return api.post('/registration', { name, email, password });
	}

	async login(email, password) {
		return api.post('/login', { email, password });
	}

	async logout() {
		return api.post('/logout');
	}
}

export default new AuthService();
