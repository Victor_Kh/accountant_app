import api from '../http';

class PartnerService {
	async getAllPartners(page, limit, search) {
		return api.get('/all_partners', {
			params: {
				page, limit, search,
			},
		});
	}

	async getSinglePartner(id) {
		return api.get(`get_partner/${id}`);
	}

	async addNewPartner(partner) {
		return api.post('/add_partner', partner);
	}

	async editPartner(partner, id) {
		return api.put(`/update_partner/${id}`, partner);
	}

	async deletePartner(id) {
		return api.delete(`/remove_partner/${id}`);
	}
}

export default new PartnerService();
