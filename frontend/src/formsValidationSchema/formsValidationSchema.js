import * as Yup from 'yup';
import 'yup-phone-lite';

const formsValidationSchema = (formType) => {
	switch (formType) {
	case 'loginForm':
		return Yup.object({
			email: Yup.string()
				.matches(/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i, 'Введіть валідну електронну адресу')
				.required('Електронна пошта обов\'язкове поле'),
			password: Yup.string()
				.min(6, 'Пароль має містити 6 або більше символів')
				.max(15, 'Пароль має містити 15 або менше символів')
				.required('Введіть ваш пароль'),
		});
	case 'registrationForm':
		return Yup.object({
			name: Yup.string()
				.min(2, 'Ім\'я має містити 2 або більше символів')
				.matches(/^\p{Letter}+$/u, 'Ім\'я повинно містити лише літери')
				.required('Введіть ваше ім\'я'),
			email: Yup.string()
				.matches(/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i, 'Введіть валідну електронну адресу')
				.required('Електронна пошта обов\'язкове поле'),
			password: Yup.string()
				.min(6, 'Пароль має містити 6 або більше символів')
				.max(15, 'Пароль має містити 15 або менше символів')
				.matches(
					/^(?=.*\d)(?=.*[A-Z])(?=.*[@#$%^&+=]).*$/,
					'Пароль повинен містити хоча б одну цифру, одну велику літеру латинського алфавіту та спеціальний символ',
				)
				.required('Введіть ваш пароль'),
			confirmedPassword: Yup.string()
				.oneOf([Yup.ref('password'), null], 'Паролі повинні бути однаковими')
				.required('Підтвердіть ваш пароль'),
		});
	case 'newPartnerForm':
		return Yup.object({
			name: Yup.string()
				.required('Це обов\'язкове поле')
				.matches(/^[a-zA-Zа-яА-Я0-9№-]+$/, 'Це поле може містити лише літери та знак № або -'),
			partnerId: Yup.string()
				.required('Це обов\'язкове поле')
				.matches(/^\d{8}(?:\d{2})?$/, 'Має містити лише 8 або 10 цифр'),
			partnerTaxNumber: Yup.string()
				.required('Це обов\'язкове поле')
				.length(10, 'ІПН має містити 10 цифр')
				.matches(/^\d+$/, 'Має містити лише цифри'),
			email: Yup.string()
				.matches(/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i, 'Введіть валідну електронну адресу')
				.required('Електронна пошта обов\'язкове поле'),
			registrationAddress: Yup.string()
				.required('Юридична адреса обов\'язкове поле'),
			actualAddress: Yup.string()
				.required('Фактична адреса обов\'язкове поле'),
			phone: Yup.string()
				.required('Це обов\'язкове поле')
				.phone('', 'Введіть валідний номер телефону'),
			banksInfo: Yup.array()
				.of(
					Yup.object({
						IBAN: Yup.string()
							.required('Це обов\'язкове поле'),
						name: Yup.string()
							.required('Це обов\'язкове поле'),
					}),
				)
				.required('')
				.min(1),
			managersInfo: Yup.array()
				.of(
					Yup.object({
						position: Yup.string()
							.required('Це обов\'язкове поле'),
						name: Yup.string()
							.required('Це обов\'язкове поле'),
						actingBy: Yup.string()
							.required('Це обов\'язкове поле'),
					}),
				)
				.required('')
				.min(1),
		});
	case 'newContractForm':
		return Yup.object({
			name: Yup.string()
				.required('Це обов\'язкове поле')
				.matches(/^[a-zA-Zа-яА-Я0-9№-]+$/, 'Це поле може містити лише літери та знак № або -'),
			partnerId: Yup.string()
				.required('Це обов\'язкове поле'),
			dateOfSign: Yup.date()
				.required('Це обов\'язкове поле'),
			dateOfEnd: Yup.date()
				.required('Це обов\'язкове поле')
				.min(Yup.ref('dateOfSign'), 'Дата закінчення не може бути до дати підписання'),
			amount: Yup.string()
				.required('Це обов\'язкове поле')
				.matches(/^\d+$/, 'Має містити лише цифри'),
			currency: Yup.string()
				.required('Це обов\'язкове поле'),
		});
	case 'newActForm':
		return Yup.object({
			name: Yup.string()
				.required('Це обов\'язкове поле')
				.matches(/^[a-zA-Zа-яА-Я0-9№-]+$/, 'Це поле може містити лише літери та знак № або -'),
			partnerId: Yup.string()
				.required('Це обов\'язкове поле'),
			dateOfSign: Yup.date()
				.required('Це обов\'язкове поле'),
			amount: Yup.string()
				.required('Це обов\'язкове поле')
				.matches(/^\d+$/, 'Має містити лише цифри'),
			currency: Yup.string()
				.required('Це обов\'язкове поле'),
			ContractId: Yup.string()
				.required('Це обов\'язкове поле'),
		});
	case 'editContractForm':
		return Yup.object({
			id: Yup.string().notRequired(),
			name: Yup.string()
				.required('Це обов\'язкове поле')
				.matches(/^[a-zA-Zа-яА-Я0-9№-]+$/, 'Це поле може містити лише літери та знак № або -'),
			partnerId: Yup.string()
				.required('Це обов\'язкове поле'),
			dateOfSign: Yup.date()
				.required('Це обов\'язкове поле'),
			dateOfEnd: Yup.date()
				.required('Це обов\'язкове поле')
				.min(Yup.ref('dateOfSign'), 'Дата закінчення не може бути до дати підписання'),
			amount: Yup.string()
				.required('Це обов\'язкове поле')
				.matches(/^\d+$/, 'Має містити лише цифри'),
			currency: Yup.string()
				.required('Це обов\'язкове поле'),
			isActive: Yup.string()
				.required('Це обов\'язкове поле'),
		});
	case 'editActForm':
		return Yup.object({
			id: Yup.string().notRequired(),
			name: Yup.string()
				.required('Це обов\'язкове поле')
				.matches(/^[a-zA-Zа-яА-Я0-9№-]+$/, 'Це поле може містити лише літери та знак № або -'),
			PartnerId: Yup.string()
				.required('Це обов\'язкове поле'),
			dateOfSign: Yup.date()
				.required('Це обов\'язкове поле'),
			amount: Yup.string()
				.required('Це обов\'язкове поле')
				.matches(/^\d+$/, 'Має містити лише цифри'),
			currency: Yup.string()
				.required('Це обов\'язкове поле'),
			isActive: Yup.string()
				.required('Це обов\'язкове поле'),
			ContractId: Yup.string()
				.required('Це обов\'язкове поле'),
		});
	default:
	}
};

export default formsValidationSchema;
